from Server.election import count
import random,os, pprint

def test_count_random(n=50, repeat_for=10**3,seed=None):
    for i in range(repeat_for):
        options = [str(i) for i in range(random.randint(4,20))]
        votes = [random.sample(options,k=random.randint(0,len(options))) for i in range(n)]
        all_participants = random.randint(len(votes), len(votes) * 1.2)
        result = count(votes,options, all_participants = all_participants)

        # On AV there is one option deleted every round
        # thus after len(options) rounds, AV has to stop.
        assert len(result["rounds"]) <= (len(options) + 2)
        assert len(result["rounds"]) > 0

        assert len(result["rounds"][-1][result["winner"]]) > all_participants/2 or result["winner"] == "NoneOfTheOtherOptions", result["rounds"][-1]
        if len(result["rounds"][-1][result["winner"]]) == all_participants/2:
            assert result["winner"] == "NoneOfTheOtherOptions"

def test_count_sepcific():
    options = ["A", "B", "C", "D"]
    votes = [["A", "B", "C"], ["B", "A"], ["A", "B"], ["C", "A", "B"]]
    result = count(votes, options)
    assert result["winner"] == "B", pprint.pformat(result["rounds"])
