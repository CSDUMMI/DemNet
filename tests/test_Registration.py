"""
Test for the Registration functionality.
"""
import string
import requests
import os
import random

TEST_SERVER_URL = os.environ["TEST_SERVER_URL"]

def test_register():
    """Test if a user can be registered without providing proper information.
    """
    errors              = requests.get(f"{TEST_SERVER_URL}/api/v0/errors").json()

    if requests.post(f"{TEST_SERVER_URL}/register").text == "Registration not enabled":
        assert True
        return True

    infos = { "username" : random_string(random.randint(10,20))
            , "password" : random_string(random.randint(10,20))
            , "first_name" : random_string(random.randint(10,20))
            , "last_name" : random_string(random.randint(10,20))
            , "email" : random_email()
            , "email-accept" : "1"
            , "accuracy" : "1"
            , "sharing" : "1"
            }

    key = random.choice(list(infos.keys()))
    params = { i: infos[i] for i in infos if i != key }
    response = requests.post(f"{TEST_SERVER_URL}/register", data = params)
    assert not response.ok
    assert response.text == "data not provided"

    key = "accept"
    infos[key] = "0"
    response = requests.post(f"{TEST_SERVER_URL}/register", params = infos)
    assert not response.ok
    assert response.text.endswith("not accepted")

def random_string(length):
    return "".join(random.choices(string.ascii_letters, k = length))

def random_email():
    codes = ["de", "com", "uk", "org", "net", "eu", "usa", "in"]
    return f"{random_string(5)}@{random_string(5)}.{random.choice(codes)}"
