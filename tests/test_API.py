# -*- coding: utf-8 -*-
import requests, os, string, random, datetime, json
from datetime import date
from Crypto.Hash import SHA256
import base64
from Server.Files import generate_cid, ALLOWED_FILES
from Server.Config import AUTHENTICITY_MARGIN

TEST_SERVER_URL = os.environ["TEST_SERVER_URL"]
GITLAB_REPO      = os.environ["GITLAB_REPO"]
USER_NAME       = os.environ["ADMIN_NAME"]
USER_PASSWORD   = os.environ["ADMIN_PASSWORD"]
TEST_MESSAGE    = int(os.environ["TEST_MESSAGE"])

def bool_to_url(query : bool):
    return "1" if query else "0"

def url_to_bool(query : str):
    return query == "1"

def test_errors():
    errors          = requests.get(f"{TEST_SERVER_URL}/api/v0/errors").json()
    assert type(errors) == dict

    for k, v in errors.items():
        assert type(k) == str
        assert type(v) == int

    inverted_errors = requests.get(f"{TEST_SERVER_URL}/api/v0/errors?invert=1").json()

    for k, v in inverted_errors.items():
        assert type(int(k)) == int
        assert type(v) == str

def login(username, password, session = None):
    if session is None:
        session = requests.Session()
    assert session.post(f"{TEST_SERVER_URL}/login", data = { "username" : username, "password" : password}).ok
    return session

def message_test(message, message_id = None, username = ""):
    assert set(message) == { "title", "content", "author", "id", "response_to", "publishing_date", "keywords", "is_public", "views", "hash" }
    if message_id != None:
        assert message["id"] == message_id
    assert type(message["title"]) == str
    assert type(message["content"]) == str

    assert type(message["keywords"]) in [str, type(None)]
    if message["keywords"] != None:
        assert all([type(keyword) == str for keyword in message["keywords"]])

    assert type(message["author"]) == dict

    user = message["author"]
    user_test(user)
    assert type(message["response_to"]) == int or message["response_to"] == None
    assert type(date.fromisoformat(message["publishing_date"])) == date

    assert message["is_public"] or message["author"]["username"] == username or username == ""
    assert message["views"] == None or message["views"] >= 0

    if message["hash"] is not None:
        assert len(message["hash"]) == 44

def user_test(user, cmp_user = {}):
    if "username" in set(cmp_user):
        assert user["username"] == cmp_user["username"]

    if "first_name" in set(cmp_user):
        assert user["first_name"] == cmp_user["first_name"]

    if "last_name" in set(cmp_user):
        assert user["last_name"] == cmp_user["last_name"]

    assert type(user["username"]) == str
    assert type(user["first_name"]) == str
    assert type(user["last_name"]) == str
    assert type(user["authentic"]) == bool
    assert type(user["accuracy"]) == float or user["accuracy"] == 0
    assert type(user["supporters"]) == list or (user["supporters"] is None and user["opponents"] is None)
    assert type(user["opponents"]) == list or (user["supporters"] is None and user["opponents"] is None)
    assert type(user["supporting"]) == list or (user["supporting"] is None and user["opposing"] is None)
    assert type(user["opposing"]) == list or (user["supporting"] is None and user["opposing"] is None)

    assert user["bio"] is None or type(user["bio"]) == str
    assert user["bio"] is None or len(user["bio"]) <= 500
    assert set(user) == { "username"
                        , "first_name"
                        , "last_name"
                        , "authentic"
                        , "accuracy"
                        , "supporters"
                        , "opponents"
                        , "supporting"
                        , "opposing"
                        , "bio"
                        }

    if user["supporters"] is not None:
        is_auth = [u["accuracy"] for u in user["supporters"]]
        is_not_auth = [u["accuracy"] for u in user["opponents"]]

        is_auth = sum(is_auth)/len(is_auth) if is_auth != [] else 0
        is_not_auth = sum(is_not_auth)/len(is_not_auth) if is_not_auth != [] else 0

        admin_users = requests.get(f"{TEST_SERVER_URL}/api/v0/user", params = { "admins" : "1" }).json()
        assert user["authentic"] == (is_auth > (is_not_auth + AUTHENTICITY_MARGIN)) or user["username"] in admin_users

    if user["supporting"] is not None:
        auth_supporting = [u for u in user["supporting"] if u["authentic"]]
        inauth_opposing = [u for u in user["opposing"] if not u["authentic"]]

        correct = len(auth_supporting) + len(inauth_opposing)
        total = len(user["supporting"]) + len(user["opposing"])
        if total == 0:
            total = 1
        assert user["accuracy"] == correct/total

    return True


def vote_test(election):
    errors          = requests.get(f"{TEST_SERVER_URL}/api/v0/errors").json()
    election["proposal_phase_start"]    = date.fromisoformat(election["proposal_phase_start"])
    election["voting_phase_start"]      = date.fromisoformat(election["voting_phase_start"])
    election["voting_phase_end"]        = date.fromisoformat(election["voting_phase_end"])

    assert set(election) == {"id", "proposal_phase_start", "voting_phase_start", "voting_phase_end", "votes", "proposals", "winner" }
    assert type(election["id"]) == int
    winner_is_int = type(election["winner"]) == int
    winner_is_nooo = election["winner"] == "NoneOfTheOtherOptions"
    election_early = election["winner"] == None and election["voting_phase_end"] >= datetime.date.today()
    assert winner_is_int or winner_is_nooo or election_early


    assert election["voting_phase_start"] > election["proposal_phase_start"]
    assert election["voting_phase_end"] > election["voting_phase_start"]

    for proposal in election["proposals"]:
        assert set(proposal) == { "description", "election_id", "id", "patch", "title", "winner", "book_proposals" }

        if not election["voting_phase_end"]:
            assert proposal["winner"] == None

        assert type(proposal["election_id"]) == int
        assert type(proposal["id"]) == int
        assert type(proposal["patch"]) == str
        assert type(proposal["title"]) == str
        assert type(proposal["description"]) == str
        assert type(proposal["book_proposals"]) == list

        for book_proposal in proposal["book_proposals"]:
            book_proposal_test(book_proposal, proposal_id = proposal["id"])

    if election["voting_phase_end"] < date.today():
        assert type(election["votes"]) == list
    else:
        assert election["votes"] == None


def test_user_get():
    username        = "dummy"

    session         = login(USER_NAME, USER_PASSWORD)

    params = { "predictions_on" : "1", "predicitions" : "1" }
    user            = session.get(f"{TEST_SERVER_URL}/api/v0/user/{username}", params = params).json()
    user_test(user, { "username" : username })

def test_user_put():
    """
    1. You can't change the data of another user.
    2. You can't change your password without providing your old password.
    3. You can change your first name and last name.
    4. You can change the bio, but everything more than 500 chars will be cut.
    """
    errors              = requests.get(f"{TEST_SERVER_URL}/api/v0/errors").json()

    session = requests.Session()
    ## Without login you can't change anything
    for i in range(2):
        user                = session.get(f"{TEST_SERVER_URL}/api/v0/user/{USER_NAME}").json()

        first_name  = ''.join(random.choices(string.ascii_letters, k = 8))
        last_name   = ''.join(random.choices(string.ascii_letters, k = 8))
        response            = session.put  (f"{TEST_SERVER_URL}/api/v0/user/{USER_NAME}"
                                            , params =
                                                { "first_name" : first_name
                                                , "last_name"  : last_name
                                                }
                                            ).json()
        if i == 0:
            session = login(USER_NAME, USER_PASSWORD, session = session)
            assert user == errors["not authenticated"]
            assert response == errors["not authenticated"]
        else:
            assert response["first_name"] == first_name
            assert response["last_name"] == last_name

    temporary_password  = ''.join(random.choices(string.ascii_letters, k = 20))

    print(temporary_password)

    change_response      = session.put(f"{TEST_SERVER_URL}/api/v0/user/{USER_NAME}"
                                      , params =  { "old_password" : USER_PASSWORD
                                                  , "new_password" : temporary_password
                                                  }
                                      ).json()

    reset_response      = session.put( f"{TEST_SERVER_URL}/api/v0/user/{USER_NAME}"
                                     , params = { "old_password" : temporary_password
                                                , "new_password" : USER_PASSWORD
                                     }).json()

    print(change_response)
    assert change_response["password_changed"]
    assert user_test({ r: change_response[r] for r in change_response if r != "password_changed" })


    assert reset_response["password_changed"]
    assert user_test({ r: response[r] for r in reset_response if r != "password_changed" })

    # Test Bio
    user = session.get(f"{TEST_SERVER_URL}/api/v0/user/{USER_NAME}").json()
    user_test(user)

    bio = user["bio"]
    new_bio = "".join(random.choices(string.ascii_letters, k = 600))

    new_user = session.put(f"{TEST_SERVER_URL}/api/v0/user/{USER_NAME}", params = { "bio" : new_bio }).json()

    session.put(f"{TEST_SERVER_URL}/api/v0/user/{USER_NAME}", params = { "bio" : bio })
    assert new_bio[:500] == new_user["bio"]

def test_message_get():
    message_id          = TEST_MESSAGE

    session = login(USER_NAME, USER_PASSWORD)

    message             = session.get(f"{TEST_SERVER_URL}/api/v0/message/{message_id}").json()

    message_test(message, message_id = message_id)

    # Check if views has incremented
    message_later       = session.get(f"{TEST_SERVER_URL}/api/v0/message/{message_id}").json()
    message_test(message, message_id = message_id)

    assert message["views"] < message_later["views"]


def test_message_put():
    errors              = requests.get(f"{TEST_SERVER_URL}/api/v0/errors").json()
    message_id          = TEST_MESSAGE

    session             = login(USER_NAME, USER_PASSWORD)

    message             = session.get(f"{TEST_SERVER_URL}/api/v0/message/{message_id}").json()

    params              = {}
    if random.uniform(0,1) > 0.5:
        params["title"]    = ''.join(random.choices(string.ascii_letters, k = 10))

    if random.uniform(0,1) > 0.5:
        params["content"]  = ''.join(random.choices(string.ascii_letters, k = 10**2))

    if random.uniform(0,1) > 0.5:
        params["keywords"] = '#'.join([''.join(random.choices(string.ascii_letters, k = 10)) for i in range(len(message["keywords"].split('#')) if message["keywords"] != None else 5)])

    if random.uniform(0,1) > 0.5:
        params["is_public"] = not message["is_public"]

    response            = session.put   (f"{TEST_SERVER_URL}/api/v0/message/{message_id}"
                                        , params = params
                                        ).json()

    assert response ==  { "response_to_changed" : False
                        , "title_changed" : ("title" in set(params))
                        , "content_changed" : ("content" in set(params))
                        , "keywords_changed" : ("keywords" in set(params))
                        , "is_public_changed" : False
                        }


    updated_message     = session.get(f"{TEST_SERVER_URL}/api/v0/message/{message_id}").json()

    if "title" in set(params):
        assert updated_message["title"] == params["title"]

    if "content" in set(params):
        assert updated_message["content"] == params["content"]

    if "keywords" in set(params):
        assert updated_message["keywords"] == params["keywords"].upper()

    if "is_public" in set(params):
        assert updated_message["is_public"] == ("1" == params["is_public"])

    session.put ( f"{TEST_SERVER_URL}/api/v0/message/{message_id}"
                , params =  { "title"   : message["title"]
                            , "content" : message["content"]
                            , "keywords": json.dumps(message["keywords"])
                            }
                )

def test_create_and_delete_message():
    errors  = requests.get(f"{TEST_SERVER_URL}/api/v0/errors").json()
    session = login(USER_NAME, USER_PASSWORD)

    title       = ''.join(random.choices(string.ascii_letters, k = 10))
    content     = ''.join(random.choices(string.ascii_letters, k = 100))
    response_to = None

    params      =   { "title" : title
                    , "content" : content
                    , "response_to" : response_to
                    }

    response    = session.post(f"{TEST_SERVER_URL}/api/v0/message", params = params).json()

    assert type(response["id"]) == int

    message_id  = response["id"]

    response    = session.delete(f"{TEST_SERVER_URL}/api/v0/message/{message_id}").json()

    assert response == errors["OK"]

    params      = { "title" : title }

    response    = session.post(f"{TEST_SERVER_URL}/api/v0/message", params = params).json()

    assert response == errors["invalid data"]

def test_messages_get():
    errors          = requests.get(f"{TEST_SERVER_URL}/api/v0/errors").json()
    all_messages    = requests.get(f"{TEST_SERVER_URL}/api/v0/message").json()
    username        = "dummy"

    for message in all_messages:
        message_test(message)

    messages_by_author  = requests.get(f"{TEST_SERVER_URL}/api/v0/message", params = { "author" : username }).json()

    print(messages_by_author)
    assert all([message["author"]["username"] == username for message in messages_by_author])
    assert len(messages_by_author) > 0

    first_20_messages = requests.get(f"{TEST_SERVER_URL}/api/v0/message?page=1").json()
    assert len(first_20_messages) == 20

def test_vote_get():
    election        = requests.get(f"{TEST_SERVER_URL}/api/v0/vote").json()
    vote_test(election)

def test_vote_post():
    errors          = requests.get(f"{TEST_SERVER_URL}/api/v0/errors").json()
    election        = requests.get(f"{TEST_SERVER_URL}/api/v0/vote").json()
    proposal_id     = os.environ["TEST_MR_ID"]
    GITLAB          = os.environ["GITLAB"]
    REPO_ID       = os.environ["REPO_ID"]

    merge_request = requests.get(f"{GITLAB}/api/v4/projects/{REPO_ID}/merge_requests/{proposal_id}").json()

    code_proposal = requests.get(f"{GITLAB_REPO}/-/merge_requests/{proposal_id}.diff").text
    book_proposals = [{ "name" : ''.join(random.choices(string.ascii_letters, k = 10))
                      , "text" : ''.join(random.choices(string.ascii_letters, k = 100))
                      } for i in range(10)
                     ]
    params = { "code_proposal" : code_proposal
             , "book_proposals" : book_proposals
             , "title" : merge_request["title"]
             , "description" : merge_request["description"]
             }
    session         = login(USER_NAME, USER_PASSWORD)


    proposal_phase_start    = date.fromisoformat(election["proposal_phase_start"])
    voting_phase_start      = date.fromisoformat(election["voting_phase_start"])
    voting_phase_end        = date.fromisoformat(election["voting_phase_end"])

    if date.today() >= proposal_phase_start and date.today() < voting_phase_start:
        unauthenticated_response    = requests.post(f"{TEST_SERVER_URL}/api/v0/vote", json = params).json()
        authenticated_response      = session.post(f"{TEST_SERVER_URL}/api/v0/vote", json = params).json()

        assert unauthenticated_response == errors["invalid user"]
        assert authenticated_response == errors["OK"] or authenticated_response == errors["invalid data"]
    else:
        out_of_time_response    = session.post(f"{TEST_SERVER_URL}/api/v0/vote", json = { "id" : proposal_id }).json()
        assert out_of_time_response == errors["invalid time"]


def test_votes():
    errors      = requests.get(f"{TEST_SERVER_URL}/api/v0/errors").json()
    elections   = requests.get(f"{TEST_SERVER_URL}/api/v0/votes").json()

    for election in elections:
            vote_test(election)

    special_date        = date.today() - datetime.timedelta(weeks = 6)
    elections_before    = requests.get(f"{TEST_SERVER_URL}/api/v0/votes?before={special_date.isoformat()}").json()

    assert all([date.fromisoformat(election["proposal_phase_start"]) < special_date for election in elections_before])

    elections_after = requests.get(f"{TEST_SERVER_URL}/api/v0/votes?after={special_date.isoformat()}").json()

    assert all([date.fromisoformat(election["proposal_phase_start"]) > special_date for election in elections_after])

    single_election = requests.get(f"{TEST_SERVER_URL}/api/v0/votes?id={elections[0]['id']}").json()

    assert len(single_election) == 1

def book_proposal_test(book_proposal, proposal_id = None):
    assert type(book_proposal["book"]) == str
    assert type(book_proposal["new_text"]) == str
    assert type(book_proposal["proposal"]) == int
    if proposal_id:
        assert book_proposal["proposal"] == proposal_id

def test_markdown_get():
    errors      = requests.get(f"{TEST_SERVER_URL}/api/v0/errors").json()
    source      = "# Hello, World!"
    html = "<h1>Hello, World!</h1>\n"
    response = requests.get(f"{TEST_SERVER_URL}/api/v0/markdown", params = { "source" : source }).json()
    assert response["html"] == html

def test_files():
    """Testing the Files API.
    1. Upload a file
    2. Delete that file.
    """
    errors      = requests.get(f"{TEST_SERVER_URL}/api/v0/errors").json()
    file        = ("ABC.txt", "".join(random.choices(string.ascii_letters, k = 10**5)), "text/plain")
    files       = { "file" : file }
    cid         = generate_cid(file[1].encode("utf-8")) + ".txt"

    session     = requests.Session()
    response    = session.post(f"{TEST_SERVER_URL}/api/v0/files", files=files).json()

    assert response == errors["not authenticated"]

    session = login(USER_NAME, USER_PASSWORD)

    user = session.get(f"{TEST_SERVER_URL}/api/v0/user/{USER_NAME}").json()

    response    = session.post(f"{TEST_SERVER_URL}/api/v0/files", files=files).json()

    if response == errors["invalid data"]:
        assert session.get(f"{TEST_SERVER_URL}/files/{cid}").text == file[1].encode("utf-8")
    else:
        response["owner"] = response["owner"]["username"]
        assert response == { "cid" : cid
                           , "url" : f"/files/{cid}"
                           , "owner" : user["username"]
                           , "name" : file[0]
                           }

        file = response
        response = session.get(f"{TEST_SERVER_URL}/api/v0/files").json()

        assert file["cid"] in list(map(lambda r: r["cid"], response))

        deleted_response = session.delete(f"{TEST_SERVER_URL}/api/v0/files", params = { "cid" : file["cid"] }).json()

        assert deleted_response == errors["OK"]

        response = session.get(f"{TEST_SERVER_URL}/api/v0/files").json()

        assert file["cid"] not in list(map(lambda r: r["cid"], response))

def test_types():
    types = requests.get(f"{TEST_SERVER_URL}/api/v0/files/types").json()
    assert types == list(ALLOWED_FILES.keys())

def rule_test(rule, accept = False):
    """A Rule:
    { "text" : <Markdown Content>
    , "name" : <Name of the Book>
    , "since" : <Date since the book is in force>
    , "accepted" : <If the user is authenticated, this is True if the user has agreed to this rule book>
    }
    """
    assert type(rule["text"]) == str
    assert type(rule["name"]) == str
    since = rule["since"]
    assert type(since) == str
    assert type(datetime.date.fromisoformat(since)) == datetime.date

    if accept:
        assert type(rule["accepted"]) == bool

def test_rules():
    """Testing the
    Rules API.
    """
    errors = requests.get(f"{TEST_SERVER_URL}/api/v0/errors").json()

    session = requests.Session()
    # Test without authenticating once, than test with.
    for i in range(2):
        rules = session.get(f"{TEST_SERVER_URL}/api/v0/rules").json()

        for rule in rules:
            rule_test(rule, accept = i == 1)
            name = rule["name"]
            rule_single_from_api = session.get(f"{TEST_SERVER_URL}/api/v0/rules/{name}").json()
            assert rule_single_from_api == rule

        invalid_rule_name = ""
        while invalid_rule_name in map(lambda r: r["name"], rules) or invalid_rule_name == "":
            invalid_rule_name = ''.join(random.choice(string.ascii_letters))


        invalid_rule = session.get(f"{TEST_SERVER_URL}/api/v0/rules/{invalid_rule_name}").json()
        assert invalid_rule == errors["data does not exist"]

        if i == 0:
            session = login(USER_NAME, USER_PASSWORD)
