import requests
import os
TEST_SERVER_URL = os.environ["TEST_SERVER_URL"]

def test_availability():
    errors = requests.get(f"{TEST_SERVER_URL}/api/v0/errors").json()
    elections = requests.get(f"{TEST_SERVER_URL}/api/v0/votes").json()

    for election in elections:
        id = election["id"]

        response = requests.get(f"{TEST_SERVER_URL}/api/v0/votes/{id}?image=1")

        if election["winner"] is None or election["votes"] is None:
            assert response.json() == errors["invalid context"]
        else:
            assert response.ok

            recv_election = response.json()
            assert "images" in response.json().keys()
            assert all(list(map(lambda i: i.startswith("/static/elections") and i.endswith(".png"),recv_election["images"].values())))

            election["images"] = recv_election["images"]
            assert election.keys() == recv_election.keys()
