"""
Postgresql database interacted using the peewee ORM.
Most changes to the database are automatically
applied once run using create_tables.
"""
import os, datetime
from peewee import *
import peeweedbevolve
from typing import List, Dict
from urllib.parse import urlparse
import requests, json
import base64

from Crypto.Hash import SHA256
from Crypto.Random import get_random_bytes

from Server.election import count
from Server.Config import AUTHENTICITY_MARGIN, JURY_SIZE

USERS_WITH_ADMIN_ACCESS = os.environ["DEMNET_ADMINS"].split(";")
ADMIN_ACCURACY = 0.95

if os.environ["APP_ENV"] == "docker":
    DATABASE        = os.environ["POSTGRES_DB"]
    PASSWORD        = os.environ["POSTGRES_PASSWORD"]
    USER            = os.environ["POSTGRES_USER"]
    HOST            = os.environ["POSTGRES_HOST"]
else:
    DATABASE_URL    = urlparse(os.environ["DATABASE_URL"])
    DATABASE        = DATABASE_URL.path[1:]
    PASSWORD        = DATABASE_URL.password
    USER            = DATABASE_URL.username
    HOST            = DATABASE_URL.hostname

length_proposal_phase   = datetime.timedelta(days = int(os.environ["LENGTH_PROPOSAL_PHASE"]))
length_voting_phase     = datetime.timedelta(days = int(os.environ["LENGTH_VOTING_PHASE"]))

database        = PostgresqlDatabase( DATABASE
                                    , password  = PASSWORD
                                    , user      = USER
                                    , host      = HOST
                                    )


class BaseModel(Model):
    class Meta():
        database    = database

class Election(BaseModel):
    id                      = AutoField()
    proposal_phase_start    = DateField()
    winner                  = TextField(null = True)
    total_users             = IntegerField(null = True)

    def propose ( self
                , title         : str
                , description   : str
                , patch         : str
                ):
        if self.in_proposal_phase():
            return Proposal.create  ( election      = self
                                    , title         = title
                                    , description   = description
                                    , patch         = patch
                                    )
        else:
            return False

    def in_proposal_phase(self):
        return datetime.date.today() >= self.proposal_phase_start and datetime.date.today() < (self.proposal_phase_start + length_proposal_phase)

    def in_voting_phase(self):
        return datetime.date.today() >= (self.proposal_phase_start + length_proposal_phase) and datetime.date.today() < (self.proposal_phase_start + length_proposal_phase + length_voting_phase)

def election_to_dict(election : Election):
    voting_phase_start  = election.proposal_phase_start + length_proposal_phase
    voting_phase_end    = election.proposal_phase_start + length_proposal_phase + length_voting_phase

    if voting_phase_end < datetime.date.today():
        votes   = []
        for vote in election.votes:
            votes.append([int(v) for v in json.loads(vote.choice)][::-1])
    else:
        votes               = None

    response    =   { "id" : election.id
                    , "proposal_phase_start"    : election.proposal_phase_start.isoformat()
                    , "voting_phase_start"      : voting_phase_start.isoformat()
                    , "voting_phase_end"        : voting_phase_end.isoformat()
                    , "proposals"               : [proposal_to_dict(proposal) for proposal in election.proposals]
                    , "votes"                   : votes
                    , "winner"                  : election.winner
                    }
    return response

class User(BaseModel):
    name        = CharField(unique = True, primary_key = True)
    first_name  = TextField()
    last_name   = TextField()
    email       = TextField(default = "", null = True)
    id          = CharField(unique = True, default = None, null = True)
    password    = CharField()
    salt        = FixedCharField(max_length = 64)

    is_auth     = DoubleField(default = 0, null = True)
    is_not_auth = DoubleField(default = 0, null = True)
    accuracy    = DoubleField(default = 0, null = True)

    bio         = CharField(max_length = 500, null = True)
    public      = BooleanField(null = True)

    def publish ( self
                , title : str
                , content : str
                , response_to : int = None
                , keywords : str    = ""
                , **kwargs
                ):
        if response_to != None:
            response_to = Message.get_by_id(int(response_to))

        message =  Message.create( author            = self
                                 , title             = title
                                 , content           = content
                                 , response_to       = response_to
                                 , publishing_date   = datetime.date.today()
                                 , keywords          = keywords
                                 , **kwargs
                                 )
        m = message_to_dict(message)
        m = { k : m[k] for k in m if k in ["title", "content", "keywords", "author"] }
        m = json.dumps(m)
        message.hash = base64.b64encode(SHA256.new(data = m.encode("utf-8")).digest(), altchars = b"-_").decode()
        message.save()
        return message

    def respond(self, title : str, content : str, response_to : int = None, **kwargs):
        return self.publish(title, content, response_to = response_to, **kwargs)

    def can_login(self, password : str) -> bool:
        password = hash_passwords(password, self.salt)
        return password == self.password

    def has_voted(self, election : Election = None):
        if election is None:
            election = current_election()

        return election.participants.select().where(Participant.user == self).exists()


    def can_vote(self, election : Election) -> bool:
        is_authentic = self.authentic()
        is_time = election.in_voting_phase()
        has_not_voted = not self.has_voted(election)
        return is_authentic and is_time and has_not_voted


    def vote(self, election : Election, choice : List[str]):
        if self.can_vote(election):
            Vote.create ( election = election
                        , choice = json.dumps(choice)
                        )
            Participant.create  ( election  = election
                                , user      = self
                                )
            return True
        else:
            return False


    def is_following(self, followed):
        try:
            follows = followed.followers.where(Follower.follower == self).exists()
        except Exception as e:
            raise e
        else:
            return follows

    def follow(self, followed):
        try:
            assert not self.is_following(followed)
            Follower.create ( follower  = self
                            , followed  = followed
                            )
        except AssertionError:
            return False
        except Exception as e:
            raise e
        else:
            return True

    def unfollow(self, followed):
        try:
            assert self.is_following(followed)

            assert Follower.delete().where(Follower.follower == self and Follower.followed == followed).execute() >= 1
        except AssertionError:
            return False
        except Exception as e:
            raise e
            return False
        else:
            return True

    def support(self, user):
        """Support the user for voting rights
        """

        self.remove_from_predictions(user)

        AuthenticityPrediction.create( user = user
                                     , author = self
                                     , supporting = True
                                     )
        self.__recalc__(user)

    def oppose(self, user):
        """Oppose the user for voting rights
        """
        self.remove_from_predictions(user)
        AuthenticityPrediction.create( user = user
                                     , author = self
                                     , supporting = False
                                     )
        self.__recalc__(user)

    def neutral(self, user):
        """Neither oppose nor support a user
        """
        self.remove_from_predictions(user)

    def remove_from_predictions(self, user):
        (AuthenticityPrediction
            .delete()
            .where(AuthenticityPrediction.user == user)
            .where(AuthenticityPrediction.author == self)
            .execute())

    def __recalc__(self, user):
        self.calc_accuracy()
        user.authentic()

    def authentic(self, recalc = True) -> bool:
        """Return True if self is authentic, otherwise False.
        If self is an admin user, this function always returns True.

        Params: recalc: True if the is_auth and is_not_auth values should be recalculated
        """
        if self.name in USERS_WITH_ADMIN_ACCESS or (self.is_auth is None or self.is_not_auth is None):
            return True


        if recalc:
            supporters = [u.author for u in self.predictions_on if u.supporting]
            opponents = [u.author for u in self.predictions_on if not u.supporting]

            supporters = [s for s in supporters if s.authentic()]
            opponents = [o for o in opponents if o.authentic()]

            self.is_auth = sum([u.accuracy for u in supporters])/len(supporters) if len(supporters) != 0 else 0
            self.is_not_auth = sum([u.accuracy for u in opponents])/len(opponents) if len(opponents) != 0 else 0
            self.save()

        if self.is_auth is None:
            self.is_auth = 1

        if self.is_not_auth is None:
            self.is_not_auth = 0

        return self.is_auth > (self.is_not_auth + AUTHENTICITY_MARGIN)

    def calc_accuracy(self, recalc = True, recalc_auth = True) -> float:
        """Return the accuracy of user in their predictions
        of the authenticity of others.
        If self is an admin user, this function always returns ADMIN_ACCURACY

        recalc = True : boolean True if the accuracy should be recalculated
        relalc_auth = True : boolean True if the authenticity should be recalculated
        """
        if self.name in USERS_WITH_ADMIN_ACCESS:
            self.accuracy = ADMIN_ACCURACY

            if self.is_dirty():
                self.save()

            return ADMIN_ACCURACY

        if recalc or self.accuracy is None:
            supporting = [u.user for u in self.predictions if u.supporting]
            opponents = [u.user for u in self.predictions if not u.supporting]

            auth_supporting = len([u for u in supporting if u.authentic(recalc = recalc_auth)])
            inauth_opposing = len([u for u in opponents if not u.authentic(recalc = recalc_auth)])


            if len(supporting) + len(opponents) == 0:
                self.accuracy = 0
            else:
                self.accuracy = (auth_supporting + inauth_opposing)/(len(supporting) + len(opponents))
            self.save()

            return self.accuracy
        else:
            return self.accuracy

def user_to_dict(user : User, supporters = False, supporting = False, recalc = False):
    if supporters:
        supporters = [user_to_dict(u.author) for u in user.predictions_on if u.supporting]
        opponents = [user_to_dict(u.author) for u in user.predictions_on if not u.supporting]
    else:
        supporters = None
        opponents = None

    if supporting:
        supporting = [user_to_dict(u.user) for u in user.predictions if u.supporting]
        opposing = [user_to_dict(u.user) for u in user.predictions if not u.supporting]
    else:
        supporting = None
        opposing = None

    return  { "username" : user.name
            , "first_name" : user.first_name
            , "last_name" : user.last_name
            , "accuracy" : user.calc_accuracy(recalc = recalc, recalc_auth = recalc)
            , "authentic" : user.authentic(recalc = recalc)
            , "supporters" : supporters
            , "opponents" : opponents
            , "supporting" : supporting
            , "opposing" : opposing
            , "bio" : user.bio
            }

class Proposal(BaseModel):
    id          = AutoField()
    election    = ForeignKeyField(Election, backref = "proposals", null = True)
    title       = TextField()
    description = TextField()
    patch       = TextField()
    link        = TextField(null = True)

def proposal_to_dict(proposal : Proposal):
    response =  { "id" : proposal.id
                , "election_id" : proposal.election.id if proposal.election else None
                , "title" : proposal.title
                , "description" : proposal.description
                , "patch" : proposal.patch
                , "winner" : proposal.election.winner == str(proposal.id) if proposal.election.winner else None
                , "book_proposals" : [book_proposal_to_dict(p) for p in proposal.book_proposals]
                }
    return response

class Follower(BaseModel):
    id          = AutoField(primary_key = True, unique = True)
    follower    = ForeignKeyField(User, backref = "following")
    followed    = ForeignKeyField(User, backref = "followers")

class Vote(BaseModel):
    election                = ForeignKeyField(Election, backref="votes", null = True)
    choice                  = TextField()
    class Meta:
        primary_key = False

class Participant(BaseModel):
    election                = ForeignKeyField(Election, backref="participants", null = True)
    user                    = ForeignKeyField(User)
    class Meta:
        primary_key = False

class Message(BaseModel):
    author                  = ForeignKeyField(User, backref = "messages")
    id                      = AutoField(primary_key = True)
    title                   = TextField()
    response_to             = ForeignKeyField('self', backref = "responses", null = True, default = None, on_delete = "SET DEFAULT")
    content                 = TextField()
    publishing_date         = DateField()
    keywords                = TextField(null = True)
    is_public               = BooleanField(default = True, null = True)
    views                   = IntegerField(null = True, default = 0)
    hash                    = FixedCharField(max_length = 44, null = True)

    def match_index(self, search : str) -> int:
        """Create a numerical value expressing how much
        of the search string matches this message.

        This is done ignorant of the words arangments.
        We split the search by it's whitespaces and
        then count the overlap with the title, content and keywords of the post.
        """
        search = set(search.split())
        title = set(self.title.split())
        content = set(self.content.split())

        title_match = len(title.intersection(search))
        content_match = len(content.intersection(search))

        if self.keywords is not None:
            keywords = set(self.keywords.split("#"))
            keywords_match = len(keywords.intersection(search))
        else:
            keywords_match = 0

        return (title_match + content_match + keywords_match)*self.id

    def excerpt(self):
        return " ".join(self.content.split()[:10])

def message_to_dict(message : Message):
    return  { "title"           : message.title
            , "content"         : message.content
            , "id"              : message.id
            , "response_to"     : message.response_to.id if message.response_to else None
            , "keywords"        : message.keywords
            , "publishing_date" : message.publishing_date.isoformat()
            , "author"          : user_to_dict(message.author)
            , "is_public"       : bool(message.is_public or message.is_public == None)
            , "views"           : message.views
            , "hash"            : message.hash
            }

class File(BaseModel):
    owner   = ForeignKeyField(User, backref = "files")
    cid     = CharField(max_length = 512)
    name    = TextField(null = True)

def file_to_dict(file : File):
    return  { "owner" : user_to_dict(file.owner)
            , "cid" : file.cid
            , "url" : f"/files/{file.cid}"
            , "name": file.name
            }

class Book(BaseModel):
    """Any Book consists of binding rules for the users,
    accepted in their most recent version by every user.
    """
    text    = TextField() # TXT of the book.
    name    = CharField(max_length = 512, unique = True, primary_key = True)
    since   = DateField()

    def excerpt(self):
        return " ".join(self.text.split()[:10])

def book_to_dict(book : Book):
    return  { "text" : book.text
            , "name" : book.name
            , "since" : str(book.since)
            }

class BookProposal(BaseModel):
    """A proposal to change a certain book.
    if the book does not exist, create a new book.
    """
    book = CharField(max_length = 512)
    proposal = ForeignKeyField(Proposal, backref = "book_proposals")
    new_text = TextField(null = True)
    applied = BooleanField(default = False, null = True)

def book_proposal_to_dict(book_proposal : BookProposal):
    return  { "book" : book_proposal.book
            , "proposal" : book_proposal.proposal.id
            , "new_text" : book_proposal.new_text
            }

class AcceptedLastChanges(BaseModel):
    """Resets every election, where the book is changed.
    """
    user = ForeignKeyField(User, backref = "agreements")
    book = ForeignKeyField(Book, backref = "agreements")
    accepted = BooleanField()

class AuthenticityPrediction(BaseModel):
    """Predictions of the author about the user.
    supporting : bool := whether the author supports or opposes the user
    """
    author = ForeignKeyField(User, backref = "predictions")
    user = ForeignKeyField(User, backref = "predictions_on")
    supporting = BooleanField()

def hash_passwords(password : str, salt : str) -> bytes:
    return SHA256.new(data = password.encode("utf-8") + salt.encode("utf-8")).hexdigest()

def register( username      : str
            , first_name    : str
            , last_name     : str
            , email         : str
            , id            : str
            , password      : str
            , connected     : bool  = False
            ):
            try:
                if not connected:
                    database.connect()

                if id is None:
                    id = User.select().count()

                salt        = SHA256.new(data = get_random_bytes(2**3)).hexdigest()
                password    = hash_passwords(password, salt)

                if User.select().where(User.id == id or User.name == user).count() > 0:
                    response    = False
                else:
                    User.create ( name          = username
                                , first_name    = first_name
                                , last_name     = last_name
                                , email         = email
                                , id            = id
                                , password      = password
                                , salt          = salt
                                )
                    response    = True
                if not connected:
                    database.close()
            except Exception as e:
                raise e
            else:
                return response

def current_election():
    """Get the most recent election.
    """
    elections = Election.select()
    elections = elections.order_by(-Election.proposal_phase_start)
    return elections.get()


def create_tables():
    try:
        assert not os.environ.get("NOT_CREATE_TABLES")
        database.connect()
        database.evolve(interactive = False)
        database.close()
    except AssertionError:
        return False
    except OperationalError:
        return False
    except Exception as e:
        raise e
    else:
        return True

create_tables()
