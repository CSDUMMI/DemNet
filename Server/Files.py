"""Variables and functions for working with files
"""
from Crypto.Hash import SHA256
ALLOWED_FILES =  { "image/png" : "png"
                 , "image/jpeg" : "jpg"

                 , "video/ogg" : "ogg"
                 , "application/ogg" : "ogg"
                 , "video/webm" : "webm"
                 , "video/mp4" : "mp4"

                 , "application/zip" : "zip"
                 , "application/x-tar" : "tar"
                 , "application/gzip" : "tar.gz"

                 , "application/pdf" : "pdf"
                 , "text/plain" : "txt"
                 }
accepted_file_types = ALLOWED_FILES.keys()
ALLOWED_FILE_EXTENSIONS = ALLOWED_FILES.values()

def generate_cid(file_content : bytes, version : str = "sha256"):
    """Create a content id for a file content.
    If no version is provided, sha256 is used.
    """
    if version == "sha256":
        hash = SHA256.new(data = file_content).hexdigest()
        return f"{version}-{hash}"
    else:
        hash = SHA256.new(data = file_content).hexdigest()
        return f"{version}-{hash}"
