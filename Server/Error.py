errors =    { "OK"                  : 1
            , "unknown error"       : 0
            , "does not exist"      : -1
            , "invalid user"        : -2
            , "invalid data"        : -3
            , "data does not exist" : -4
            , "data not provided"   : -5
            , "invalid time"        : -6
            , "invalid context"     : -7
            , "invalid data format" : -8
            , "not authenticated"   : -9
            }
