"""Application Programming Interface
for authenticated users.
This uses the Flask-RESTful Module,
which maps Resource classes to urls
and calls the methods on those classes
corresponding to the methods of the requests to those urls.

All URLs defined at the bottom are bound
after /api/v0/.
All responses are JSON
and the error codes returned can be reversed
to a textual representation using the
errors dictionary in Server.Error
or the same dictionary returned by
the /api/v0/errors route.
"""
import os, datetime, requests, subprocess, json, re
from flask import Blueprint, jsonify, session, request, redirect, Response
from flask_restful import Resource, Api, reqparse
import flask
import urllib.parse
import traceback

import Server.Database as db
from Server.Error import errors
from Server.Markdown import markdown
import Server.Files as files
from clock import init

import werkzeug
from werkzeug.utils import secure_filename
from peewee import *
from playhouse.shortcuts import model_to_dict

plt = None

api_app         = Blueprint("api", __name__)
api             = Api(api_app)

USERS_WITH_ADMIN_ACCESS = os.environ["DEMNET_ADMINS"].split(";")
GITLAB_URI              = os.environ["GITLAB_URI"]
GITLAB_TOKEN            = os.environ["GITLAB_TOKEN"]
DEMNET_ID               = os.environ["DEMNET_ID"]
DEMNET_LOCATION         = os.environ["DEMNET_LOCATION"]
ADMIN_USERS             = os.environ["DEMNET_ADMINS"].split(";")

"""URL Conventions:
1 := True
anything else := False
"""
def to_bool(x : str):
    return x == "1"

def none_is_true(x):
    return x == None or x

def none_is_false(x):
    return bool(x)

class User(Resource):
    """Resource to get and manipulate a single user.
    """
    def get(self, username):
        """Arguments:
        - recalc : bool if the authenticity and  accuracy should be recalculated
        - predictions : bool if the predictions (supporting and opposing) should be returned
        - predictions_on : bool if the predictions on this user (supporters and opponents) should be returned.
        Example response:
        {
            "first_name" : "Dummy",
            "last_name" : "Dummy",
            "username" : "dummy",
            "authentic" : false,
            "accuracy" : 0
            "supporters" : [<SUPPORTERS>],
            "opponents" : [<OPPONENTS>],
            "supporting": [<USER SUPPORTED BY dummy>],
            "opposing" : [<USER OPPOSED BY dummy]
        }
        These errors may occur:
        - "not authenticated" if the user is not logged in.
        - "does not exist" if there is no user with that username.
        - "unknown error" if an error occures, that we haven't predicted.
        """
        try:
            assert session.get("authenticated", default = False), "not authenticated"

            parser = reqparse.RequestParser()
            parser.add_argument("recalc", type = to_bool)
            parser.add_argument("predictions", type = to_bool)
            parser.add_argument("predictions_on", type = to_bool)
            args = parser.parse_args()

            user = db.User.get_by_id(username)
            response = db.user_to_dict(user, supporters = args["predictions_on"], supporting = args["predictions"], recalc = args["recalc"])

        except AssertionError as error:
            response = errors["not authenticated"]

        except DoesNotExist:
            response    = errors["does not exist"]

        except Exception as e:
            response    = errors["unknown error"]
            raise e

        return jsonify(response)

    def put(self, username):
        """
        Arguments:
        - old_password : str is required if you want to change your password to the new_password
        - new_password : str the new password to change to, if the old_password is valid
        - first_name   : str the new first name to change yours to.
        - last_name    : str the new last name to change yours to.
        - has_vote     : boolean the new state of the has_vote flag
        - support      : List<str> usernames to support (encoded as JSON)
        - oppose       : List<str> usernames to oppose (encoded as JSON)
        - neutral      : List<str> usernames to neither oppose nor support (encoded as JSON)
        - bio          : str new bio

        If support, oppose or neutral contradict, the behaviour is undefined.

        Example response:
        {
            "username" : "dummy",
            "first_name" : "Dummy",
            "last_name" : "Dummy",
            "password_changed" : errors["invalid data"]
        }
        Possible values for password_changed field:
        - "invalid data" if the old_password is not valid. And only then.
        - "OK" otherwise

        These errors may occur:
        - "invalid user" if the logged in user and the user that is attempted to be changed don't match or the client isn't authenticated.
        - "unknown error" if an error occured, which we are not predicting.
        """
        try:
            assert session.get("authenticated", False), "not authenticated"
            assert session["username"] == username, "invalid user"

            user    = db.User.get_by_id(session["username"])

            parser  = reqparse.RequestParser()
            parser.add_argument("old_password", type = str, help = "Old password, needed to change to a new password")
            parser.add_argument("new_password", type = str, help = "New password, to change to.")
            parser.add_argument("first_name", type = str, help = "New first name")
            parser.add_argument("last_name", type = str, help = "New last name")
            parser.add_argument("has_vote", type = to_bool)
            parser.add_argument("support", type = json.loads)
            parser.add_argument("oppose", type = json.loads)
            parser.add_argument("neutral", type = json.loads)
            parser.add_argument("bio", type = str)
            args    = parser.parse_args()

            auth_success = False
            if args["old_password"] and args["new_password"]:
                auth_success = user.can_login(args["old_password"])
                if auth_success:
                    user.password   = db.hash_passwords(args["new_password"], user.salt)

            if args["first_name"]:
                user.first_name = args["first_name"]

            if args["last_name"]:
                user.last_name  = args["last_name"]

            if args["support"]:
                for sup in args["support"]:
                    try:
                        user.support(db.User.get_by_id(sup))

                    except DoesNotExist:
                        continue

            if args["oppose"]:
                for ops in args["oppose"]:
                    try:
                        user.oppose(db.User.get_by_id(ops))
                    except DoesNotExist:
                        continue

            if args["neutral"]:
                for neutral in args["neutral"]:
                    try:
                        user.neutral(db.User.get_by_id(neutral))
                    except DoesNotExist:
                        continue

            if args["bio"] is not None:
                bio = args["bio"][:500]
                user.bio = bio

            if user.is_dirty():
                user.save()

            response    = db.user_to_dict(user, supporting = True)
            response["password_changed"] = auth_success

        except AssertionError as error:
            response    = errors[error.args[0]]
        except Exception as e:
            response    = errors["unknown error"]
            raise e

        return jsonify(response)

    def post(self, username):
        """Register a new user.
        This requires that the user is logged in as one of the administrating users.
        Arguments:
        - username : str (required) the username of the new user
        - first_name : str (required) the first legal name of the new user
        - last_name : str (required) the last legal name of the new user
        - id : str (required) ID string of the new user
        - password : str (required) Password of the new user.

        Example Response:
        {
            "username" : "dummy",
            "first_name" : "Dummy",
            "last_name" : "Dummy"
        }

        These errors can occur:
        - "invalid user" if the user is not a user with admin access.
        - "data not provided" if one of required arguments are not provided.
        - "invalid data" if there is a user with the same username or id.
        - "unknown error" if an error occured, which we didn't expect.
        """
        try:
            assert False, "does not exist"
            assert session.get("authenticated", default = False), "not authenticated"
            assert session.get("username", default = "") in USERS_WITH_ADMIN_ACCESS, "invalid user"

            parser  = reqparse.RequestParser()
            parser.add_argument("first_name", type = str, required = True, help = "Legally correct first name of the user")
            parser.add_argument("last_name", type = str, required = True, help = "Legally correct last name of the user")
            parser.add_argument("id", type = str, required = True, help = "Identification")
            parser.add_argument("password", type = str, required = True, help = "Password of the new user")
            args    = parser.parse_args()

            assert db.register  ( username      = username
                                , first_name    = args["first_name"]
                                , last_name     = args["last_name"]
                                , id            = args["id"]
                                , password      = args["password"]
                                , connected     = True
                                ), "invalid data"

            response =  { "username"    : username
                        , "first_name"  : args["first_name"]
                        , "last_name"   : args["last_name"]
                        }

        except werkzeug.exceptions.BadRequest:
            response    = errors["data not provided"]
        except AssertionError as error:
            response    = errors[error.args[0]]
        except Exception as e:
            raise e
            response    = errors["unknown error"]
        finally:
            return jsonify(response)

class Users(Resource):
    """Resource to fetch public users. (Admin Users)
    """
    def get(self):
        """Get public users:
        - admins : bool if you want admin users
        Returns:
        [<USERS>]
        """
        try:
            parser  = reqparse.RequestParser()
            parser.add_argument("admins", type = to_bool)
            args    = parser.parse_args()

            users = []
            if args["admins"]:
                users.extend([db.User.get_by_id(u) for u in ADMIN_USERS if db.User.select().where(db.User.name == u).exists()])
            response = jsonify([db.user_to_dict(user) for user in users])
        except werkzeug.exceptions.BadRequest:
            response = errors["data not provided"]

        return response


class Message(Resource):
    """Resource to fetch, manipulate and delete Messages.
    """

    def get(self, message_id):
        """Example response:
        {
          "author": {
            "first_name": "Joris",
            "last_name": "Gutjahr",
            "username": "joris"
          },
          "content": "This Server is experimental, all data maybe lost.\r\n",
          "id": 1,
          "publishing_date": "2020-03-28",
          "response_to": null,
          "title": "About this Server",
          "keywords" : null,
          "is_public" : true
        }

        These errors can occur:
        - "does not exist" if the message referenced doesn't exist.
        - "invalid user" if the message isn't public.
        - "unknown error" if an error occures that we don't know.
        """
        try:
            message = db.Message.get_by_id(message_id)

            if message.views != None:
                message.views += 1
            else:
                message.views = 0

            message.save()
            response = db.message_to_dict(message)
        except AssertionError:
            response = errors["invalid user"]
        except db.DoesNotExist:
            response    = errors["does not exist"]
        except Exception as e:
            response    = errors["unknown error"]
            raise e
        finally:
            return jsonify(response)

    def put(self, message_id):
        """Update/Edit a message
        Request Arguments:
        - title : str the new title of the post
        - content : str the new content of the post
        - response_to : int the id of the post, that this post should now respond to.
        - keywords : str the new keywords string of the post.
        - is_public : str is interpreted as True == "1" and is the new is_public value.
        If is_public == "1", the post will made public if it isn't already. Otherwise it is made not public.

        Example response:
        { "title_changed" : true
        , "content_changed" : true
        , "response_to_changed" : true
        , "keywords_changed" : false
        , "is_public_changed" : false
        }

        These errors can occur:
        - "invalid data format" if the keywords string isn't a properly formatted JSON List.
        - "invalid user" if the authenticated user isn't the author.
        - "unknown error" if any error occures, we don't know of.
        """
        try:
            message = db.Message.get_by_id(message_id)
            assert session.get("username", None) == message.author.name

            parser  = reqparse.RequestParser()
            parser.add_argument("title", type = str, help = "New title")
            parser.add_argument("content", type = str, help = "New content")
            parser.add_argument("response_to", type = int, help = "Changed response to")
            parser.add_argument("keywords", type = str, help = "New keywords list")
            parser.add_argument("is_public", type = to_bool)
            args    = parser.parse_args()

            response    =   { "title_changed" : False
                            , "content_changed" : False
                            , "response_to_changed" : False
                            , "keywords_changed" : False
                            , "is_public_changed" : False
                            }

            if args["title"]:
                message.title = args["title"]
                response["title_changed"] = True

            if args["content"]:
                message.content = args["content"]
                response["content_changed"] = True

            if args["keywords"]:
                message.keywords = args["keywords"].upper()
                response["keywords_changed"] = True

            if args["response_to"]:
                try:
                    message_to_respond_to  = db.Message.get_by_id(args["response_to"])
                    message.response_to = message_to_respond_to
                    response["response_to_changed"] = True
                except db.DoesNotExist:
                    response["response_to_changed"] = False


            if args["is_public"] != None and args["is_public"] != none_is_true(message.is_public):
                message.is_public = args["is_public"]
                response["is_public_changed"] = True

            message.save()
        except AssertionError:
            response = errors["invalid user"]
        except ValueError:
            response    = errors["invalid data format"]
        except Exception as e:
            raise e
            response    = errors["unknown error"]
        finally:
            status = 400 if type(response) == int else 200
            response = flask.make_response(jsonify(response), status)
            return response

    def delete(self, message_id):
        """Deleting the message if the client is the author.
        """
        try:
            message = db.Message.get_by_id(message_id)
            assert session.get("username", None) == message.author.name and session.get("authenticated")

            message.delete_instance()
            response    = errors["OK"]
        except AssertionError:
            response = errors["invalid user"]
        except db.DoesNotExist:
            response = errors["does not exist"]
        except Exception as e:
            response = errors["unknown error"]
            raise e
        finally:
            return jsonify(response)

class Messages(Resource):
    """To create a new mesage or get all existing ones.
    """
    def get(self):
        """
        Get a filtered list of all posts.
        Arguments:
        - search (not required) search string to ILIKE with the message title, content or keywords
        - author (not required) only those posts from an author (username)
        - page (not required) given if one should only show posts on a given page
        - page_size (not required) defaults to 20 and defines the size of the pages.
        Only relevant if page is also given.
        Example output:
        [
            {
                "author": {
                  "first_name": "Joris",
                  "last_name": "Gutjahr",
                  "username": "joris"
                },
                "content": "E and E and E. It is the most frequent character. And thus important\n\nThis is a propsal for the current election",
                "id": 83,
                "publishing_date": "2020-05-13",
                "response_to": null,
                "title": "Replaced with E",
                "keywords" : null,
                "is_public" : true
            }
        ]

        These errors can occur:
        - "data does not exist" if a username given as author doesn't exist.
        - "invalid user" if not logged in and requesting with drafts = "1"
        - "unknown error", if any other error occures.
        """
        try:
            parser      = reqparse.RequestParser()
            parser.add_argument("search", type = str, help = "Search query string")
            parser.add_argument("author", type = str, help = "Restrict the messages to only those by a certain user")
            parser.add_argument("page", type = int)
            parser.add_argument("page_size", type = int)
            parser.add_argument("is_public", type = to_bool)
            args        = parser.parse_args()

            messages    = db.Message.select().order_by(-db.Message.id)

            if args["author"]:
                author = db.User.get_by_id(args["author"])
                messages = messages.where(db.Message.author == author)

            if args["search"]:
                messages = (messages.where(db.Message.title.contains(args["search"]))
                                    .orwhere(db.Message.content.contains(args["search"]))
                                    .orwhere(db.Message.keywords.contains(args["search"]))
                        )

            if args["page"]:
                if args["page_size"] is None:
                    args["page_size"] = 20
                messages = messages.paginate(args["page"], paginate_by = args["page_size"])

            response   = [db.message_to_dict(message) for message in messages]
        except AssertionError:
            response = errors["invalid user"]
        except db.DoesNotExist:
            response = errors["data does not exist"]
        except Exception as error:
            if os.environ.get("DEBUG", default = False):
                raise error
            response = errors["unknown error"]
        finally:
            return jsonify(response)

    def post(self):
        """Arguments:
        - title (required), the title of the new message
        - content (required), the content of the new message
        - response_to (not required), ID of the message, that this new message is responding to.
        - keywords (not required) String separated by # and prefixed by one of keywords.
        - is_public (defaults = 1) 1 if the post should be public 0 othwerise.
        Example Response:
        {
          "author": {
            "first_name": "Joris",
            "last_name": "Gutjahr",
            "username": "joris"
          },
          "content": "Post content",
          "id": 123,
          "publishing_date": "2020-06-18",
          "response_to": null,
          "title": "New Post"
        }

        These errors can occur:
        - "invalid data", if title or content is not provided
        - "data does not exist", if there is no message for the provided response_to id
        - "unknown error", if any other error occures.
        """
        try:
            assert session.get("authenticated", False)

            parser  = reqparse.RequestParser()
            parser.add_argument("title", type = str, help = "Title of the new message", required = True)
            parser.add_argument("content", type = str, help = "Content of the new message", required = True)
            parser.add_argument("response_to", type = int, help = "ID of the message, that the new message responds to")
            parser.add_argument("keywords", type = str)
            parser.add_argument("is_public", type = lambda x: x == "1")
            args    = parser.parse_args()

            author = db.User.get_by_id(session["username"])
            message = author.publish( args["title"]
                                    , args["content"]
                                    , response_to = args["response_to"]
                                    , keywords = args["keywords"]
                                    , is_public = args["is_public"] if args["is_public"] is not None else True
                                    )

            response = db.message_to_dict(message)

        except werkzeug.exceptions.BadRequest:
            response    = errors["invalid data"]
        except db.DoesNotExist:
            response    = errors["data does not exist"]
        except AssertionError as e:
            response    = errors["invalid user"]
        except Exception as e:
            raise e
            response    = errors["unknown error"]
        finally:
            return jsonify(response)

class Vote(Resource):
    def get(self):
        """Get the state of the current election.
        Arguments: none
        Example Response:
        {
          "id": 11,
          "proposal_phase_start": "2020-06-20",
          "proposals": [
            {
              "description": "",
              "election_id": 11,
              "id": 41,
              "patch": "From 588f3654505229d6ac01ea7e3c1857d1bb798428 Mon Sep 17 00:00:00 2001\nFrom: Joris <joris.gutjahr@protonmail.com>\nDate: Sat, 20 Jun 2020 10:25:10 +0000\nSubject: [PATCH] Update README.md\n\n---\n README.md | 2 +-\n 1 file changed, 1 insertion(+), 1 deletion(-)\n\ndiff --git a/README.md b/README.md\nindex 9b56258..e9da105 100644\n--- a/README.md\n+++ b/README.md\n@@ -1,3 +1,3 @@\n # Test\n \n-Q\n\\ No newline at end of file\n+A\n\\ No newline at end of file\n-- \nGitLab\n\n",
              "title": "Ultimatly A",
              "winner": null,
              "book_proposals" : [<BOOKS changed>]
            }
          ],
          "votes": null,
          "voting_phase_end": "2020-07-18",
          "voting_phase_start": "2020-07-04"
        }
        These errors may occur:
        - "unknown error" if an unpredicted error occured
        """
        try:
            current_election    = db.Election.select().order_by(-db.Election.proposal_phase_start).get()
            response    = db.election_to_dict(current_election)
        except Exception as e:
            raise e
            response    = errors["unknown error"]
        finally:
            return jsonify(response)

    def post(self):
        """Arguments:
        Body:
        { "title" : <Title of the proposal>
        , "description" : <Description of the proposal>
        , "code_proposal" : <diff to apply. Required.>
        , "book_proposals" :
                [{ "name" : <NAME of the Book>
                , "text" : <The new content>
                }]
        }
        Response:
        - errors["OK"]
        These errors may occur:
        - "data not provided" if the code_proposal argument isn't given
        - "invalid user" if the client isn't logged in
        - "invalid data" if the code_proposal, title or description are not provided.
        - "invalid time" if currently the election isn't in a proposal phase
        - "invalid context" if the same patch was already proposed.
        """
        try:
            assert session.get("authenticated", False), "invalid user"
            user        = db.User.get_by_id(session["username"])
            election    = db.current_election()

            args = request.get_json()

            assert election.in_proposal_phase(), "invalid time"

            book_proposals = args["book_proposals"]

            title = args["title"]
            description = args["description"]
            patch = args["code_proposal"]

            assert title != "" and description != "" and patch != "", "invalid data"

            if book_proposals != []:
                assert not election.proposals.where(db.Proposal.patch == patch).exists(), "invalid context"


            init()
            subprocess.run( ["git apply --check --whitespace=fix"]
                          , input = patch.encode("utf-8")
                          , shell = True
                          , check = True
                          , cwd   = os.environ["GIT_REPOSITORY"]
                          )

            if election.proposals.where(db.Proposal.title == title).exists():
                same_title_proposals = election.proposals.where(db.Proposal.title.regexp(f"{title}( [0-9])?"))
                index = map(lambda p: re.match(f"{title}( [0-9])?", p.title), same_title_proposals)
                index = map(lambda m: 0 if m[1] is None else int(m[1]), index)
                index = max(index) + 1
                title = f"{title} ({index})"

            proposal    = election.propose( title       = title
                                          , description = description
                                          , patch       = patch
                                          )

            assert proposal != False, "invalid time"

            books = { "new" : []
                    , "changed" : []
                    , "removals" : []
                    }

            for book_proposal in book_proposals:
                name = book_proposal["name"]
                new_text = book_proposal["text"]

                if db.Book.select().where(db.Book.name == name).exists():
                    book = db.Book.get_by_id(name)

                    if new_text == "":
                        books["removals"].append(book.name)
                    else:
                        message = user.publish(book["name"], book["text"], keywords=f"#election{election.id}#{proposal.title}#change#book")
                        books["changed"].append({ "name" : book["name"], "message" : message.id })

                else:
                    message = user.publish(book["name"], book["text"], keywords=f"#election{election.id}#{proposal.title}#new#book")
                    books["new"].append({ "name" : book["name"], "message" : message.id })

                book_proposal = db.BookProposal.create( book = name
                                                      , new_text = new_text
                                                      , proposal = proposal
                                                      )
                book_proposal.save()

            new = ""
            changed = ""
            removals = ""

            for book in books["changed"] + books["new"]:
                book

            for new_book in books["new"]:
                new += f"\n-[{new_book['name']}](/read/{new_book['message']})"

            for changed_book in books["changed"]:
                changed += f"\n-[{changed_book['name']}](/read/{changed_book['message']})"

            for remove_book in books["removals"]:
                removals += f"\n- {remove_book}"

            content = f"""{description}
### Rule Books
#### New Books
{new}
#### Changes
{changed}
#### Removals
{removals}
This is a proposal for election #{election.id}
            """
            user.publish(title, content)

            response = errors["OK"]

        except KeyError as error:
            response    = errors["data not provided"]
        except werkzeug.exceptions.BadRequest:
            response    = errors["data not provided"]
        except AssertionError as e:
            response    = errors[e.args[0]]
        except subprocess.CalledProcessError:
            response    = errors["invalid data"]
        except Exception as error:
            print(error)
            traceback.print_exc()
            response = errors["unknown error"]
        finally:
            return jsonify(response)

    def put(self):
        """Arguments:
        - "choice" a JSON List wher index 0 is the first choice, 1 the second, etc. If the choice ends before all options are placed, the vote eventually goes to NoneOfTheOtherOptions.
        Example Response:
        - errors["OK"]
        Possible erros:
        - "invalid data" if the JSON Array in choice is malformatted.
        - "invalid context" if the user has already voted.
        - "invalid user" if the user isn't authenticated.
        - "invalid time" if the current election isn't yet open for voting.
        - "data not provided" if choice argument isn't given.
        - "unknown error" if an error occures, that we haven't predicted.

        """
        try:
            assert session.get("authenticated", False), "invalid user"
            user                = db.User.get_by_id(session["username"])
            current_election    = db.Election.select().order_by(-db.Election.proposal_phase_start).get()

            assert current_election.in_voting_phase(), "invalid time"

            parser              = reqparse.RequestParser()
            parser.add_argument("choice", type = json.loads, required = True, help = "A JSON Array of the possible options sorted from most favorite to least.")
            choice              = parser.parse_args()["choice"]

            assert user.vote(current_election, choice), "invalid context"
            response            = errors["OK"]

        except werkzeug.exceptions.BadRequest:
            response    = errors["data not provided"]
        except AssertionError as e:
            response    = errors[e.args[0]]
        except Exception as e:
            raise e
            response    = errors["unknown error"]
        finally:
            return jsonify(response)

class Votes(Resource):
    """Get all elections and search them, somewhat.
    """
    def get(self):
        """Arguments:
        - before : date.fromisoformat only return elections before a given date.
        - after : date.fromisoformat only return elections after a given date.
        - id : int only return a list of a single element, the election with the given id.
        Example Response:
        [
          {
            "id": 11,
            "proposal_phase_start": "2020-06-20",
            "proposals": [
              {
                "description": "",
                "election_id": 11,
                "id": 41,
                "patch": "From 588f3654505229d6ac01ea7e3c1857d1bb798428 Mon Sep 17 00:00:00 2001\nFrom: Joris <joris.gutjahr@protonmail.com>\nDate: Sat, 20 Jun 2020 10:25:10 +0000\nSubject: [PATCH] Update README.md\n\n---\n README.md | 2 +-\n 1 file changed, 1 insertion(+), 1 deletion(-)\n\ndiff --git a/README.md b/README.md\nindex 9b56258..e9da105 100644\n--- a/README.md\n+++ b/README.md\n@@ -1,3 +1,3 @@\n # Test\n \n-Q\n\\ No newline at end of file\n+A\n\\ No newline at end of file\n-- \nGitLab\n\n",
                "title": "Ultimatly A",
                "winner": null,
                "book_proposals" : []
              },
              {
                "description": "QUod?",
                "election_id": 11,
                "id": 42,
                "patch": "From 9a6013ef0f838545c5a860fcf447a55ef34bcb09 Mon Sep 17 00:00:00 2001\nFrom: Joris <joris.gutjahr@protonmail.com>\nDate: Sun, 21 Jun 2020 14:17:50 +0000\nSubject: [PATCH] QMD\n\n---\n README.md | 2 +-\n 1 file changed, 1 insertion(+), 1 deletion(-)\n\ndiff --git a/README.md b/README.md\nindex 9b56258..90121c8 100644\n--- a/README.md\n+++ b/README.md\n@@ -1,3 +1,3 @@\n # Test\n \n-Q\n\\ No newline at end of file\n+QMD\n\\ No newline at end of file\n-- \nGitLab\n\n",
                "title": "QMD",
                "winner": null,
                "book_proposals" : []
              }
            ],
            "votes": null,
            "voting_phase_end": "2020-07-18",
            "voting_phase_start": "2020-07-04"
          }
        ]


        These errors may occur:
        - "unknown error" for errors that we didn't expect.
        """
        try:
            parser      = reqparse.RequestParser()
            parser.add_argument("before", type = datetime.date.fromisoformat)
            parser.add_argument("after", type = datetime.date.fromisoformat)
            parser.add_argument("id", type = int)
            args = parser.parse_args()

            elections   = db.Election.select()

            if args["id"]:
                elections = elections.where(db.Election.id == args["id"])

            if args["before"]:
                elections = elections.where(db.Election.proposal_phase_start < args["before"])

            if args["after"]:
                elections = elections.where(db.Election.proposal_phase_start > args["after"])

            response    = [db.election_to_dict(election) for election in elections]
        except Exception as e:
            raise e
            response    = errors["unknown error"]
        finally:
            return jsonify(response)

class VotesArchive(Resource):
    def get(self, id):
        """Arguments:
        - image : bool := True if image should be sent.
        - round : int := if image: visualise a specific round. default = 0
        Response:
        Election Object with images field, if images are requested.
        """
        try:
            parser = reqparse.RequestParser()
            parser.add_argument("image", type = to_bool)
            parser.add_argument("round", type = int, default = 0)
            args = parser.parse_args()

            election = db.Election.get_by_id(id)

            response =  db.election_to_dict(election)

            if args["image"] is not None and response["votes"] is None:
                response = errors["invalid context"]

            elif args["image"]:
                def to_round(image):
                    return image.split("-")[1].split(".png")[0]
                image_url = draw_election(args["round"] if args["round"] else 0, election)

                images = filter(lambda s: s.startswith(f"{id}-"), os.listdir("static/elections"))
                images = { to_round(image) : f"/static/elections/{image}" for image in images }
                response["images"] = images

        except DoesNotExist:
            response = errors["data does not exist"]
        except Exception as e:
            raise e

        return response


def draw_election(round, election):
    global plt
    if plt is None:
        import matplotlib.pyplot as plt
        
    if os.path.isfile(f"static/elections/{election}-{round}.png"):
        return f"/static/elections/{election}-{round}.png"

    proposals = [p.id for p in election.proposals]
    votes = [json.loads(v.choice) for v in election.votes]

    if votes == []:
        return False

    if round >= max(map(len,votes)):
        return False

    votes = [v for v in votes if len(v) >= round]
    none_of_the_other_options = len(election.votes) - len(votes)
    votes = [v[round] for v in votes]

    sizes = [votes.count(p)/(len(election.votes) if len(election.votes) != 0 else 1) * 100 for p in proposals]
    sizes += [none_of_the_other_options/len(election.votes)] if len(election.votes) != 0 else [0]

    labels = [p.title for p in election.proposals]
    labels += ["None of the other Options"]

    fig, ax = plt.subplots()
    (wedges, texts, autotexts) = ax.pie(sizes, autopct = "%1.1f%%", startangle=90)

    ax.legend(wedges, labels, title = "Proposals", loc = "center left", bbox_to_anchor = (1,0,0.5,1))
    plt.setp(autotexts, size = 8, weight = "bold")
    ax.set_title(f"Election #{election.id} Round: {round}")
    ax.axis("equal")
    fig.tight_layout()
    fig.savefig(f"static/elections/{election.id}-{round}.png", pad_inches = 2)
    plt.show()
    return f"/static/elections/{election}-{round}.png"

class Markdown(Resource):
    """Markdown conversion.
    """
    def get(self):
        """Arguments:
        - source : The Markdown source code
        Response:
        - source = "# Hello, World"
        { "html" : "<h1>Hello, World</h1>\n"
        }
        Errors:
        - "unknown error"
        - "data not provided" if source not provided.
        """
        try:
            parser = reqparse.RequestParser()
            parser.add_argument("source", required = True)
            args = parser.parse_args()
            response = { "html" : markdown(args["source"]) }
        except werkzeug.exceptions.BadRequest:
            response = errors["data not provided"]
        except Exception as e:
            raise e
            response = errors["unknown error"]
        finally:
            return jsonify(response)

class Files(Resource):
    """Resource to upload, download and delete a **single** file.
    """
    def get(self):
        """Get the Files CIDs and URIs a user has uploaded.
        Only available to that user.
        Response:
        [
            { "cid" : <CID>
            , "url" : "/files/<CID>"
            , "owner" : { "username" : <USERNAME>
                        , "first_name" : <FIRST NAME>
                        , "last_name" : <LAST NAME>
                        }
            }
        ]
        Errors:
        - "not authenticated" if the user is not logged in.
        - "unknown error"
        """
        try:
            assert session.get("authenticated", default = False), "not authenticated"

            user = db.User.get_by_id(session["username"])
            response = list(map(db.file_to_dict, user.files))

        except AssertionError as error:
            response = errors[error.args[0]]
        except Exception as error:
            raise error
        finally:
            return jsonify(response)

    def post(self):
        """Create a new file as an attachment to a post.
        Arguments:
        - refer (optional): relative url to redirect to after a sucessful upload.
        Body:
        - file : The file to upload (Content-Type header must be set.)
        Response:
        { "url" : "/files/<cid>"
        , "cid" : <cid>
        , "owner" : { "username" : <USERNAME>
                    , "first_name" : <FIRST NAME>
                    , "last_name" : <LAST NAME>
                    }
        }
        Possible Errors:
        - "not authenticated" if the user is not authenticated (logged in).
        - "invalid data" if the file already exists. (Or a file with that content)
        - "invalid data format" if the Content-Type of the file is not accepted. files.accepted_file_types
        - "unknown error"
        """
        try:
            parser = reqparse.RequestParser()
            parser.add_argument ( "file"
                                , type=werkzeug.datastructures.FileStorage
                                , location="files"
                                , required=True
                                )
            parser.add_argument("refer", type=str)
            args = parser.parse_args()

            assert session.get("authenticated", default = False), "not authenticated"

            user = db.User.get_by_id(session["username"])

            file = args["file"]

            assert file.mimetype in files.ALLOWED_FILES.keys(), "invalid data format"

            real_filename = secure_filename(file.filename)
            content = file.read()
            cid = f"{files.generate_cid(content)}.{files.ALLOWED_FILES[file.mimetype]}"
            file = db.File.create(owner = user, cid = cid, name = real_filename)
            filename = os.path.join("/files", secure_filename(cid))
            response = db.file_to_dict(file)

            open(filename, mode = "xb").write(content)


        except FileExistsError:
            response = errors["invalid data"]

        except db.DoesNotExist:
            response = errors["data does not exist"]

        except AssertionError as error:
            response = errors[error.args[0]]

        except Exception as error:
            response = errors["unknown error"]
            print(error)

        finally:
            if args["refer"] is not None and args["refer"].startswith("/"):
                return redirect(args["refer"])
            else:
                return jsonify(response)


    def delete(self):
        """Delete a file uploaded with post.
        Arguments:
        - cid : CID returned by POST Route.
        Example Response:
        errors["OK"]
        Errors:
        - "not authenticated" if the user is not logged in.
        - "invalid user" if the user is not the author of the attachment.
        - "unknown error"
        """
        try:
            assert session.get("authenticated", default = False), "not authenticated"

            parser = reqparse.RequestParser()
            parser.add_argument("cid", required = True)
            args = parser.parse_args()

            cid = args["cid"]

            user = db.User.get_by_id(session["username"])
            files = db.File.select().where(db.File.cid == cid)

            users_files = files.where(db.File.owner == user)
            assert users_files.exists() or not files.exists(), "invalid user"

            if users_files.count() == files.count() and users_files.exists():
                os.remove(f"/files/{users_files.get().cid}")

            db.File.delete().where(db.File.owner == user).where(db.File.cid == cid).execute()
            response = errors["OK"]

        except db.DoesNotExist:
            response = errors["data does not exist"]
        except Exception as error:
            raise error
        else:
            return jsonify(response)

class Rules(Resource):
    def get(self):
        """
        Response:
        [ <book as in Rule.get>
        ]
        Errors:
        - "unknown error"
        """
        try:
            response = [get_rule(b) for b in db.Book.select()]
        except Exception as error:
            raise error
        else:
            return jsonify(response)

class Rule(Resource):
    def get(self, name):
        """
        Response:
        { "text" : <Markdown Content>
        , "name" : <Name of the Book>
        , "since" : <Date since the book is in force>
        , "accepted" : <If the book has been agreed to, as long as the user is authenticated>
        }
        Errors:
        - "data does not exist"
        """
        try:
            response = db.Book.get_by_id(name)
            response = get_rule(response)
        except db.DoesNotExist:
            response = errors["data does not exist"]
        except Exception as error:
            raise error
        finally:
            return jsonify(response)

    def post(self, name):
        """
        Accept this rule.
        Response: errors["OK"]
        Errors:
        - "not authenticated" if the user is not authenticated.
        - "data does not exist" if the book does not exist.
        - "unknown error"
        """
        try:
            assert session.get("authenticated", default = False), "not authenticated"
            user = db.User.get_by_id(session["username"])
            book = db.Book.get_by_id(name)

            agreement = user.agreements.where(db.AcceptedLastChanges.book == book).get()
            if not agreement.accepted:
                agreement.accepted = True
                agreement.save()

            response = errors["OK"]
        except AssertionError as error:
            response = errors[error.args[0]]
        finally:
            return jsonify(response)

def get_rule(book):
    """Returns:
    { "text" : <Markdown Content>
    , "name" : <Name of the Book>
    , "since" : <Date since the book is in force>
    , "accepted" : <If the book has been agreed to, as long as the user is authenticated>
    }
    """
    response = db.book_to_dict(book)
    if session.get("authenticated", default = False):
        user = db.User.get_by_id(session["username"])
        agreement = user.agreements.where(db.AcceptedLastChanges.book == book).get()
        response["accepted"] = agreement.accepted
    return response

# ADDING RESOURCES
api.add_resource(User, "/user/<string:username>")
api.add_resource(Users, "/user")
api.add_resource(Message, "/message/<int:message_id>")
api.add_resource(Messages, "/message")
api.add_resource(Vote, "/vote")
api.add_resource(Votes, "/votes")
api.add_resource(VotesArchive, "/votes/<int:id>")
api.add_resource(Markdown, "/markdown")
api.add_resource(Files, "/files")
api.add_resource(Rules, "/rules")
api.add_resource(Rule, "/rules/<string:name>")

@api_app.route("/errors", methods=["GET"])
def errors_download_point():
    try:
        invert = to_bool(request.values.get("invert", False))
        response = errors
        if invert:
            response = {v: k for k, v in response.items() }

    except Exception as e:
        raise e
        response = "An unknown server error occured"
    finally:
        return jsonify(response)

@api_app.route("/files/types", methods=["GET"])
def types_download_point():
    try:
        response = list(files.ALLOWED_FILES.keys())
    except Exception as error:
        raise error
    else:
        return jsonify(response)
