"""Module to provide the project's markdown parser.
The markdown function provided here is to be used everywhere
markdown has to be parsed in this project.
"""
import mistune

def parse_video(self, match, state):
    """parsing anything of the form:
    !vid[<alt>](<sources list (separator: ,)>)
    """
    alt = match.group(1)
    sources = match.group(2).split(",")
    return "video", alt, sources

def render_video(alt, sources):
    """Recursive renderer
    """
    srcs = ""
    for src in sources:
        srcs += f"<source src=\"{mistune.escape(src)}\" />"
    alt = markdown(alt)[3:-5]
    return f"<video controls>{srcs}{alt}</video>"


def plugin_video(md):
    """Create a plugin to embed HTML5 videos
    in markdown documents.
    """
    md.inline.register_rule("video", r"!vid\[(.*)\]\(([\w -_:,]+)\)", parse_video)

    md.inline.rules.append("video")

    if md.renderer.NAME == "html":
        md.renderer.register("video", render_video)

markdown = mistune.create_markdown(renderer = "html", plugins=[plugin_video])
