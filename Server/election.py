import random, pprint, copy
from typing import List, Dict

noneOfTheOtherOptions = "NoneOfTheOtherOptions"
def current_choice(vote : List[str]):
    if vote == []:
        return noneOfTheOtherOptions
    else:
        return vote[-1]

def count(votes : List[List[str]], options : List[str], all_participants : int = 0):
    options  : Dict[str, List[List[str]]] = { key : list(filter(lambda v: current_choice(v) == key, votes)) for key in list(options) }
    options = { key : list(map(lambda v: [a for a in v if a in list(options)], options[key])) for key in list(options) }
    votes   = len(votes)

    if all_participants == 0:
        all_participants = votes

    options[noneOfTheOtherOptions] = [[] for i in range(all_participants - votes)]
    options[noneOfTheOtherOptions].extend([[] for i in range(sum([options[key].count([]) for key in list(options)]))])
    options = { key : list(filter(lambda v: v != [] or key == noneOfTheOtherOptions, options[key])) for key in list(options) }

    result = { "winner" : None
             , "rounds" : []
             }
    while result["winner"] == None:
        # Does a candidate have more than 50%?
        result["rounds"].append(options)
        winner = next((key for key in list(options) if len(options[key]) > (0.5*all_participants)), None)
        if winner:
            result["winner"] = winner
            break
        elif all([len(options[key]) == 0.5*votes for key in list(options)]):
            result["winner"] = noneOfTheOtherOptions
            break
        else:
            # Is there only one option left?
            if len(list(options)) == 1:
                result["winner"] = list(options)[0]
                break
            elif len(list(options)) == 0:
                result["winner"] = noneOfTheOtherOptions
                break
            else:
                # Drop worst candidate and find out who voters liked next best
                options_without_noon = filter(lambda o: o != noneOfTheOtherOptions, list(options))

                worst = max(list(options_without_noon), key = lambda o: -len(options[o]))
                worsts_votes = options.pop(worst)
                worsts_votes = [v[0:-1] for v in worsts_votes]
                options = { key : options[key] + [v for v in worsts_votes if current_choice(v) == key] for key in list(options) }
                options = { key : [list(filter(lambda a: a != worst, vote)) for vote in options[key]] for key in list(options)}
                options[noneOfTheOtherOptions].extend([[] for i in range(sum([options[key].count([]) for key in list(options)]))])
                options = { key : [v for v in options[key] if v != [] and key != noneOfTheOtherOptions ] for key in list(options)}
                continue

    result["rounds"].append(copy.copy(options))
    return result
