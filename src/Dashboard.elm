module Dashboard exposing (..)
import DemNet.File as F
import File exposing (File)
import File.Select as Select
import Http
import Html exposing (Html, div, text, button)
import Html.Events exposing (onClick)
import Html.Attributes as Attributes exposing (style)
import Octicons
import Browser
import Json.Decode as D
import Dict exposing (Dict)
import Time
import Element exposing (Element)
import Element.Input as Input
import Element.Border as Border

-- MAIN
main =
  Browser.element
    { init = init
    , view = view
    , update = update
    , subscriptions = subscriptions
    }

-- MODEL
type alias Model
  = { files : List F.File
    , errors : Dict String Int
    , mime_types : List String
    }

getErrors : Cmd Msg
getErrors
  = Http.get
    { url = "/api/v0/errors"
    , expect = Http.expectJson GotErrors (D.dict D.int)
    }

getTypes : Cmd Msg
getTypes
  = Http.get
      { url = "/api/v0/files/types"
      , expect = Http.expectJson GotTypes (D.list D.string)
      }

init : () -> (Model, Cmd Msg)
init _ =
  ( { files = []
    , errors = Dict.empty
    , mime_types = []
    }
  , Cmd.batch [ F.get LoadedFiles
              , getErrors
              , getTypes
              ]
  )

-- UPDATE
type Msg
  = LoadFiles Time.Posix
  | LoadedFiles (Result Http.Error (List F.File))
  | DeleteFile F.File
  | DeletedFile F.File (Result Http.Error Int)
  -- Setup and configuration requested on init
  | GotErrors (Result Http.Error (Dict String Int))
  | GotTypes (Result Http.Error (List String))

update : Msg -> Model -> (Model, Cmd Msg)
update msg model
  = case msg of
      LoadFiles _ ->
        (model, F.get LoadedFiles)
      LoadedFiles result ->
        case result of
          Ok files -> ({ model | files = files }, Cmd.none)
          Err _ ->  (model, F.get LoadedFiles)

      DeleteFile file ->
        (model, F.delete DeletedFile file)
      DeletedFile file result ->
        case result of
          Ok i -> if codeIs "OK" i model.errors then ({ model | files = List.filter (\f -> f /= file) model.files}, Cmd.none) else (model, Cmd.none)
          Err _ -> (model, F.delete DeletedFile file)
      GotErrors result ->
        case result of
          Ok errors -> ({ model | errors = errors}, Cmd.none)
          Err _ -> (model, getErrors)
      GotTypes result ->
        case result of
          Ok types -> ({ model | mime_types = types}, Cmd.none)
          Err _ -> (model, getTypes)

codeIs : String -> Int -> Dict String Int -> Bool
codeIs text code errors =
  case Dict.get text errors of
    Just c -> c == code
    Nothing -> False

-- SUBSCRIPTIONS
subscriptions : Model -> Sub Msg
subscriptions model =
  Time.every 2000 LoadFiles

-- VIEW
view : Model -> Html Msg
view model =
  List.map viewFile model.files
  |> Element.column [Element.width Element.fill]
  |> Element.layout [ Border.width 1
                    , Border.rounded 5
                    , Element.width Element.fill
                    ]
  |> List.singleton
  |> Html.div [style "padding" "1vw"]

viewFile : F.File -> Element Msg
viewFile file =
  let link = Element.newTabLink [Element.width Element.fill]
              { url = file.url
              , label = Element.row [Element.width Element.fill]
                          [ Element.text <|
                            case file.filename of
                              Just name -> name
                              Nothing -> file.cid
                          , Octicons.defaultOptions
                              |> Octicons.size 24
                              |> Octicons.linkExternal
                              |> Element.html
                          ]
              }
  in Element.row
        [ Element.spaceEvenly
        , Element.width Element.fill
        , Border.width 1
        , Border.solid
        , Element.padding 0
        , Element.width Element.fill
        ]
        [ link
        , Input.button []
                       { onPress = Just <| DeleteFile file
                       , label = Element.text "Delete"
                       }
        ]
