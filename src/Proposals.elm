port module Proposals exposing (..)
{-| Module for proposing.
-}
import Http
import Browser
import Dict
import Html exposing (Html)
import Element exposing (Element, el, text)
import Element.Input as Input
import Element.Font as Font
import Element.Background as Background
import Element.Border as Border
import DemNet.Book as Book exposing (Book(..), get_rules)
import DemNet.Proposal as Proposal exposing ( Proposal
                                            , empty
                                            , propose
                                            )
import DemNet.Date as Date
import DemNet.Errors as Errors exposing (Errors)
import DemNet.Settings exposing (maintainer)
import Octicons exposing (size, pencil, defaultOptions)
import Markdown

-- MAIN
main = Browser.element
        { init = init
        , update = update
        , subscriptions = \_ -> Sub.none
        , view = view
        }

-- MODEL
type alias Model
  = { proposal : Proposal
    , books : List Book
    , new_book : Book
    , errors : Errors
    , error_code : Maybe Int
    }

init : () -> (Model, Cmd Msg)
init _ = ({ proposal = { textProposals = Dict.empty
                       , codeProposal = ""
                       , title = ""
                       , description = ""
                       }
          , books = []
          , new_book = Book.empty
          , errors = Errors.empty
          , error_code = Nothing
          }, Cmd.batch [ get_rules Received
                       , Errors.get ReceivedErrors
                       ])

-- PORTS
port reload : () -> Cmd msg

-- UPDATE
type alias Name = String
type Msg
  = Received (Result Http.Error (List Book))
  | ReceivedErrors (Result Http.Error Errors)
  | ChangeTitle String
  | ChangeDescription String
  | ChangeText Book Name String
  | ChangeCode String
  | Remove Book
  | Propose
  | Proposed Proposal (Result Http.Error Int)
  | ToggleView Book
  | Edit Book
  | New
  | New_Name String
  | Add_New

update : Msg -> Model -> (Model, Cmd Msg)
update msg model
  = case msg of
      Received result ->
        case result of
          Ok books -> ({ model | books = books }, Cmd.none)
          Err _ -> (model, get_rules Received)

      ReceivedErrors result ->
        case result of
          Ok errors -> ({ model | errors = errors}, Cmd.none)
          Err _ -> (model, Errors.get ReceivedErrors)

      ChangeTitle title ->
        let proposal = model.proposal
            new_proposal = { proposal | title = title }
        in ({ model | proposal = new_proposal }, Cmd.none)

      ChangeDescription description ->
        let proposal = model.proposal
            new_proposal = { proposal | description = description }
        in ({ model | proposal = new_proposal }, Cmd.none)

      ChangeText (Book book) name text ->
        let new_book = Book { book | text = text }
            proposal = model.proposal
            new_proposal = { proposal | textProposals = Dict.insert name new_book (proposal.textProposals) }
        in
          ( { model | proposal = new_proposal }
          , Cmd.none)
      ChangeCode text ->
        let proposal = model.proposal
        in ( { model | proposal = { proposal | codeProposal = text }}
           , Cmd.none
           )
      Remove (Book book) ->
        let new_book = Book <| { book | text = "" }
            books = Dict.insert book.name new_book <| model.proposal.textProposals
            proposal = model.proposal
            new_proposal = { proposal | textProposals = books }
        in ({ model | proposal = new_proposal }, Cmd.none)

      Propose ->
        if not <| empty model.proposal
          then (model, propose (Proposed model.proposal) model.proposal)
          else (model, Cmd.none)

      Proposed proposal result ->
        case result of
          Ok i -> if Errors.fromText "OK" model.errors == Just i
                    then (model, reload ())
                    else ({ model | error_code = Just i }, Cmd.none)
          Err _ ->  (model, propose (Proposed model.proposal) model.proposal)

      ToggleView (Book book) ->
        ({ model | books = replace (Book book) (Book { book | view_text = not book.view_text }) model.books }
        , Cmd.none
        )
      Edit book ->
        let proposal = Proposal.add book model.proposal
        in ({ model | proposal = proposal }, Cmd.none)
      New ->
        ({ model | new_book = Book.empty }, Cmd.none)

      New_Name name ->
        let (Book new_book) = model.new_book
        in ({ model | new_book = Book <| { new_book | name = name } }, Cmd.none)

      Add_New ->
        ( { model | proposal = Proposal.add model.new_book model.proposal
                  , new_book = Book.empty
                  , books = model.new_book::model.books
                  }
        , Cmd.none )

replace : a -> a -> List a -> List a
replace a b
  = List.map (\x -> if a == x then b else x)

-- VIEW
view : Model -> Html Msg
view model
  = [ case model.error_code of
        Just code ->
          case Errors.toText code model.errors of
            Just txt -> case txt of
              "data not provided" -> text "No diff or patch provided"
              "invalid user" -> text "You are not logged in"
              "invalid data" -> text "Is the title, description or diff empty? Or is the diff does not apply."
              "invalid time" -> text "It's not proposal phase. You cannot propose"
              "invalid context" -> text "This proposal has already been proposed"
              "OK" -> Element.none
              _ -> Element.link []
                      { url = maintainer
                      , label = text "You have found a bug in the system. Contact the maintainer."
                      }
            Nothing -> Element.link []
                    { url = maintainer
                    , label = text "You have found a bug in the system. Contact the maintainer."
                    }
        Nothing -> Element.none

    , viewProposal model
    , addBook model
    , Input.button
        button_attrs
        { onPress = Just Propose
        , label = text "Propose"
        }
    ]
  |> Element.column [ Element.paddingXY 50 5
                    , Border.solid
                    , Border.rounded 5
                    , Border.width 1
                    , Element.height Element.shrink
                    , Element.spaceEvenly
                    ]
  |> Element.layout [ Element.width Element.fill
                    ]

-- VIEW UTILS
button_attrs : List (Element.Attribute Msg)
button_attrs
       = [ Border.solid
         , Border.width 1
         ]

addBook : Model -> Element Msg
addBook model
  = [ let (Book new_book) = model.new_book
      in Input.text []
            { onChange = New_Name
            , text = new_book.name
            , placeholder = Just << Input.placeholder [] <| text "Name of a new book"
            , label = Input.labelHidden "Name of a new book"
            }
    , Input.button button_attrs
        { onPress = Just <| Add_New
        , label = let (Book new_book) = model.new_book
                  in text <| "Add Book \"" ++ new_book.name ++ "\""
        }
    ] |> Element.row []



viewProposal : Model -> Element Msg
viewProposal model
  = let books = List.map
                  (\(Book b) -> case Dict.get b.name model.proposal.textProposals of
                          Just new_b -> new_b
                          Nothing -> Book b)
                  model.books
    in [ el [Font.size 24] <| text "New Proposal"
       , Input.text []
          { onChange = ChangeTitle
          , text = model.proposal.title
          , placeholder = Just << Input.placeholder [] <| text "Title"
          , label = Input.labelRight [] <| text "Title"
          }
       , Input.multiline []
          { onChange = ChangeDescription
          , text = model.proposal.description
          , placeholder = Just << Input.placeholder [] <| text "Description"
          , label = Input.labelRight [] <| text "Description"
          , spellcheck = True
          }
        , { onChange = ChangeCode
          , text = model.proposal.codeProposal
          , placeholder = Just <| Input.placeholder [] (text "diff or patch file")
          , label = Input.labelAbove [] << el [Font.size 24] <| text "Code"
          , spellcheck = False
          } |> Input.multiline [Font.family [Font.monospace] ]
        , Element.column [] <| List.map (viewProposalBook model) books
        ] |> Element.column [ Element.width Element.fill ]

viewProposalBook : Model -> Book -> Element Msg
viewProposalBook model (Book book)
  = if List.member (Book book) <| Dict.values model.proposal.textProposals
      then Element.column
            []
            [ { onChange = ChangeText (Book book) book.name
              , text = book.text
              , placeholder = Just <| Input.placeholder [] (text "Article 1.")
              , label = Input.labelAbove [] <| el [Font.size 24] <| text book.name
              , spellcheck = True
              } |> Input.multiline []
            ]
      else Element.column
            [ Element.width Element.fill
            , Element.spaceEvenly
            ]
            [ Element.row []
             [ text book.name
                |> el [Font.size 24]
                |> (\label -> Input.button button_attrs
                                { onPress = Just << ToggleView <| Book book
                                , label = label
                                })
             , defaultOptions
                 |> size 24
                 |> pencil
                 |> Element.html
                 |> (\label -> Input.button button_attrs
                                   { onPress = Just << Edit <| Book book
                                   , label = label
                                   })
             , defaultOptions
                |> size 24
                |> Octicons.x
                |> Element.html
                |> (\label -> Input.button
                                button_attrs
                                { onPress = Just << Remove <| Book book
                                , label = label
                                })
             ]
            , if book.view_text
                then Markdown.toHtml [] book.text
                      |> Element.html
                else Element.none
            ]
