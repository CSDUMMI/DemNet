module Authentication exposing (..)

import Browser
import Html exposing (Html)
import Element exposing (Element, el, text, fill, fillPortion, shrink, width, height, paragraph, textColumn, column, px)
import Element.Input as Input
import Element.Background as Background
import Element.Font as Font
import Element.Border as Border
import Markdown

import DemNet.User as User exposing (User)
import Http

-- MAIN
main = Browser.element
  { init = init
  , update = update
  , subscriptions = subscriptions
  , view = view
  }

-- MODEL
type alias Model =
  { user : User
  , self : User
  , message : String
  , edit : Bool
  }

init : (String, String) -> (Model, Cmd Msg)
init (username, self_username) =
  ({ user = User.init username
   , self =  User.init self_username
   , message = ""
   , edit = False
   },
   let same = username == self_username
       cmds_ = [User.fetch True username Recv_User]
       cmds = if same
                then cmds_
                else (User.fetch False self_username Recv_User)::cmds_
   in Cmd.batch cmds
  )

-- UPDATE
type Msg
  = Support
  | Neutral
  | Oppose
  | Recv_User (Result Http.Error User)
  | ChangeBio String
  | BioChanged (Result Http.Error ())
  | Edit

update : Msg -> Model -> (Model, Cmd Msg)
update msg model
  = case msg of
      Support -> (model, User.support model.user model.self Recv_User)
      Neutral -> (model, User.neutral model.user model.self Recv_User)
      Oppose -> (model, User.oppose model.user model.self Recv_User)
      Recv_User result ->
        case result of
          Err err -> ({ model | message = "An Error occured"}, Cmd.none)
          Ok user -> (if user.username == model.user.username
                        then { model | user = user }
                        else { model | self = user }, Cmd.none)
      ChangeBio new ->
        let user_ = model.user
            user = { user_ | bio = String.left 500 new }
        in ({ model | user = user }, User.upload_bio user BioChanged)
      BioChanged _ -> (model, Cmd.none)
      Edit -> ({ model | edit = not model.edit }, Cmd.none)

-- SUBSCRIPTIONS
subscriptions _ = Sub.none

-- VIEW
view : Model -> Html Msg
view model
  = Element.layout [height shrink]
    <| Element.column []
       [ text model.message
       , Element.html <| Markdown.toHtml [] model.user.bio
       , if model.user.username == model.self.username
           then view_bio model
           else view_authentication model
       ]


view_bio : Model -> Element Msg
view_bio model
  = column [Element.paddingEach { top = 0, bottom = 50, right = 0, left = 0 }]
    [ Input.button
      [Background.color <| Element.rgb255 50 40 252
      , width <| px 50
      , height <| px 50
      , Border.rounded 50
      , Font.center
      ]
      { onPress = Just Edit
      , label = text <| if model.edit then "Save" else "Edit"
      }
    , if model.edit
        then Input.multiline []
          { onChange = ChangeBio
          , text = model.user.bio
          , placeholder = Just << Input.placeholder [] <| text "Something about myself, that I want the world to know."
          , label = Input.labelAbove [] << text <| "Bio (" ++ (String.fromInt <| String.length model.user.bio) ++ "/500)"
          , spellcheck = True
          }
        else Element.none
    ]

view_authentication : Model -> Element Msg
view_authentication model
  = Element.column []
     [ paragraph [] [text "Do you support this user or oppose this user as authentic. Or are you neutral about them?"]
     , Element.wrappedRow [Element.spaceEvenly, height shrink, width <| px 200]
         [ view_support_button model
         , view_oppose_button model
         ]
     ]


view_support_button : Model -> Element Msg
view_support_button model
  = let is_supporting = User.is_supporting model.user model.self
    in view_button [width <| fillPortion 1]
        is_supporting Support "Neutral" "Support"

view_oppose_button : Model -> Element Msg
view_oppose_button model
  = let is_opposing = User.is_opposing model.user model.self
    in view_button [width <| fillPortion 1]
        is_opposing Oppose "Neutral" "Oppose"

view_button : List (Element.Attribute Msg) -> Bool -> Msg -> String -> String -> Element Msg
view_button attr is_current_state msg in_state_label out_of_state_label
  = let attrs = [ Background.color <| Element.rgb255 204 0 204
                , Font.color <| Element.rgb255 255 255 255
                , Border.color <| Element.rgb255 204 0 204
                , Border.width 5
                , Font.center
                , Border.rounded 50
                ]
    in Input.button (attrs ++ attr)
      { onPress = Just <| if is_current_state then Neutral else msg
      , label = text <| if is_current_state then in_state_label else out_of_state_label
      }
