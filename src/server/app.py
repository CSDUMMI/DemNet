"""DemNet API
"""
from flask import Flask, request, jsonify
from ariadne import graphql_sync
from .schema import schema

app = Flask(__name__)

@app.route("/graphql", methods=["POST"])
def graphql():
    data = request.get_json()

    success, result = graphql_sync(
        schema,
        data,
        context_value=request,
        debug=app.debug
    )

    status_code = 200 if success else 400
    return jsonify(result), status_code
