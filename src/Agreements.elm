port module Agreements exposing (..)
import Browser
import Http
import Html exposing (Html)
import Json.Decode as D
import Element exposing ( Element
                        , row
                        , column
                        , textColumn
                        , paragraph
                        , spacing
                        , width
                        , height
                        , centerX
                        , fill
                        , el
                        , text
                        , paddingXY
                        , padding
                        , spaceEvenly
                        )
import Element.Font as Font
import Element.Input as Input
import Element.Events as Events
import Element.Background as Background
import Element.Border as Border
import DemNet.Date as Date exposing (Date)
import DemNet.Book as Book exposing (Book(..), decoder, get_rules, accept)
import Parser
import Time exposing (Posix)
import Dict exposing (Dict)
import Markdown

import DemNet.Colors as Colors

-- MAIN
main = Browser.element
        { init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        }

-- MODEL
type alias Model
  = { books : Dict String Book  }

init : () -> (Model, Cmd Msg)
init _ = ({ books = Dict.empty}, get_rules Received)

-- PORTS
port redirectHome : () -> Cmd msg

-- UPDATE
type Msg
    = Received (Result Http.Error (List Book))
    | Accept Book
    | Accepted Book (Result Http.Error Int)
    | Show Book
    | Hide Book
    | Submit

update : Msg -> Model -> (Model, Cmd Msg)
update msg model
  = case msg of
      Received result ->
        case result of
          Ok books -> ({ model | books = List.foldl (\(Book b) acc -> Dict.insert b.name (Book b) acc) Dict.empty books }
            , Cmd.none)
          Err err -> (model, get_rules Received)
      Accepted (Book book) result ->
        case result of
          Ok i -> ({ model | books = Dict.insert book.name  (Book { book | accepted = Just True }) model.books }
                  , Cmd.none
                  )
          Err _ -> (model, Cmd.none)
      Accept book -> (model, accept Accepted book)
      Show (Book book) -> ({ model | books = Dict.insert book.name (Book { book | view_text = True }) model.books }
                          , Cmd.none)
      Hide (Book book) ->
            ({ model | books =
              Dict.insert book.name (Book { book | view_text = False}) model.books
             }
            , Cmd.none)
      Submit -> if all_books_accepted model
                  then (model, redirectHome ())
                  else (model, Cmd.none)

all_books_accepted : Model -> Bool
all_books_accepted =
  Dict.isEmpty
  << Dict.filter (\name book -> book.accepted /= Just True)
  << Dict.map (\name (Book book) -> book)
  << .books

-- SUBSCRIPTIONS
subscriptions : Model -> Sub Msg
subscriptions _ = Sub.none

-- VIEW
view : Model -> Html Msg
view model =
  Element.layout [ paddingXY 0 50
                 , spaceEvenly
                 , centerX
                 , Font.center
                 ]
    <| column [ height fill
              , width fill
              ]
    <| let books = if model.books == Dict.empty
                      then [el  [ Font.size 24
                                , Background.color <| Element.rgb 3 109 74
                                , Font.color <| Element.rgb 255 255 255
                                ] <| text "There are no books."
                           ]
                      else List.map toElement <| Dict.values model.books
           submit = if all_books_accepted model
                      then Colors.button []
                              { onPress = Just Submit
                              , label = text "Go Home"
                              }
                      else Element.none
        in books ++ [submit]

toElement : Book -> Element Msg
toElement (Book book) =
  textColumn
      [padding 50, Font.center, centerX]
      [ if book.view_text
        then paragraph
              ([Events.onClick << Hide <| Book book] ++  Colors.details_attrs)
              [ el [Font.size 24] <| text book.name
              , Element.html <| Markdown.toHtml [] book.text
              ]
        else Colors.details_open [Font.center, centerX]
                          { onPress = Just << Show <| Book book
                          , label = el [Font.size 24, Font.center] <| text book.name
                          }
      , el [] << text <| "First approved by the users: " ++ Date.nice_encode book.since_in_force
      , if book.accepted == Just True
            then Element.none
            else Colors.button []
                  { onPress = Just << Accept <| Book book
                  , label = text <| "Accept " ++ book.name
                  }
      ]
