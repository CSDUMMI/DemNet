port module Editor exposing (..)
import Browser
import Json.Decode as D
import Http
import Html exposing (Html)
import Element exposing (Element, column, width, height, fill, fillPortion, centerY, centerX, spacing, text)
import Element.Input as Input
import Element.Border as Border
import Element.Background as Background
import Element.Font as Font
import Url.Builder as Builder

import DemNet.Colors exposing (button_color, button_font_color)

-- MAIN
main =
  Browser.element
    { init = init
    , view = view
    , update = update
    , subscriptions = subscriptions
    }


-- MODEL
type alias Model
  = { title : String
    , content : String
    , keywords : String
    , id : Maybe Int
    , response_to : Maybe Int
    , message : String
    }

decoder : D.Decoder Model
decoder =
  D.map5 (\t c k i r -> Model t c (Maybe.withDefault "" k) i r "")
    (D.field "title" D.string)
    (D.field "content" D.string)
    (D.field "keywords" <| D.nullable D.string)
    (D.field "id" <| D.nullable D.int)
    (D.field "response_to" <| D.nullable D.int)

init : (Int, Int) -> (Model, Cmd Msg)
init (id, response_to) = ({ title = ""
           , content = ""
           , keywords = ""
           , id = if id == -1 then Nothing else Just id
           , response_to = if response_to == -1 then Nothing else Just response_to
           , message = ""
           }
          , if id /= -1
              then fetch id
              else Cmd.none
          )
-- PORTS
port storage : ({ title : String, content : String, keywords : String } -> msg) -> Sub msg
port save : Model -> Cmd msg
port redirect : () -> Cmd msg
port showMd : String -> Cmd msg

-- UPDATE
type Field
  = Title
  | Content
  | Keywords

type Msg
  = Change Field String
  | Upload
  | Uploaded (Result Http.Error ())
  | Fetched (Result Http.Error Model)
  | Store { title : String, content : String, keywords : String }
  | RecvMd String (Result Http.Error String)


update : Msg -> Model -> (Model, Cmd Msg)
update msg model
 = case msg of
    Change field new ->
      let new_model = case field of
              Title -> { model | title = new }
              Content -> { model | content = new }
              Keywords -> { model | keywords = new }
      in (new_model, Cmd.batch [save new_model, markdown new_model])

    Upload ->
      (model, upload model )

    Uploaded result ->
      case result of
        Ok () -> (model, redirect ())
        Err _ -> ({ model | message = "Couldn't upload. An Error occured" }, Cmd.none)

    Fetched result ->
      case result of
        Ok new_model -> (new_model, Cmd.none)
        Err _ -> ({ model | message = "Couldn't fetch. An Error occured" }, Cmd.none)

    Store message ->
      ({ model | title = message.title, content = message.content, keywords = message.keywords }
      , Cmd.none)

    RecvMd md result ->
      case result of
        Ok html -> (model, if md == model.content then showMd html else Cmd.none)
        Err _ -> (model, Cmd.none)

-- SUBSCRIPTIONS
subscriptions : Model -> Sub Msg
subscriptions _ = storage Store


-- VIEW

view : Model -> Html Msg
view model =
  Element.layout []
    <| column [ width fill, height fill, Element.paddingXY 50 0, spacing 10]
           [ Input.spellChecked [Font.size 24,Input.focusedOnLoad]
              { onChange = Change Title
              , text = model.title
              , placeholder = Just << Input.placeholder [] <| text "Title ..."
              , label = Input.labelLeft [] <| text "Title"
              }
           , Input.multiline [height <| fillPortion 6, width <| fillPortion 2]
              { onChange = Change Content
              , text = model.content
              , placeholder = Just << Input.placeholder [] <| text "Content ..."
              , label = Input.labelAbove [] <| text "Content"
              , spellcheck = True
              }
           , Input.text []
              { onChange = Change Keywords
              , text = model.keywords
              , placeholder = Just << Input.placeholder [] <| text "#topic#other topic"
              , label = Input.labelLeft [] <| text "Keywords"
              }
           , Element.el [Font.size 14] <|  text model.message
           , Input.button
              [ Background.color button_color
              , Border.color button_color
              , Border.width 10
              , Font.color button_font_color
              , centerX
              ]
              { onPress = Just Upload
              , label = text <| if model.id == Nothing then "Publish" else "Publish Changes"
              }
           ]

-- HTTP
upload : Model -> Cmd Msg
upload model
  = let path = ["api", "v0", "message"]
        query_ = [ Builder.string "title" model.title
                 , Builder.string "content" model.content
                 , Builder.string "keywords" model.keywords
                 ]
        query = case model.response_to of
          Just r -> query_ ++ [Builder.int "response_to" r]
          Nothing -> query_
    in case model.id of
      Just id ->
        Http.request
          { method = "PUT"
          , headers = []
          , url = Builder.absolute (path ++ [String.fromInt id]) query
          , body = Http.emptyBody
          , expect = Http.expectWhatever Uploaded
          , timeout = Nothing
          , tracker = Nothing
          }
      Nothing ->
        Http.post
          { url = Builder.absolute path query
          , body = Http.emptyBody
          , expect = Http.expectWhatever Uploaded
          }

fetch : Int -> Cmd Msg
fetch id
  = let path = ["api", "v0", "message", String.fromInt id]
        query = []
    in Http.get
        { url = Builder.relative path query
        , expect = Http.expectJson Fetched decoder
        }

markdown : Model -> Cmd Msg
markdown model
  = let source = "# " ++ model.title ++ "\n" ++ model.content
    in Http.get
      { url = Builder.relative ["api", "v0", "markdown"] [Builder.string "source" source]
      , expect = Http.expectJson (RecvMd model.content) (D.field "html" D.string)
      }
