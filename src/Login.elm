port module Login exposing (..)
import Browser
import Browser.Dom as Dom
import Browser.Events
import Json.Decode as Decode
import Http
import Html exposing (Html)
import Html.Events
import Element exposing (Element, column, width, height, fill, fillPortion, centerY, centerX, spacing, el, text, paragraph)
import Element.Input as Input
import Element.Border as Border
import Element.Background as Background
import Element.Font as Font
import Url.Builder as Builder

import DemNet.Colors as Colors exposing (button_color, button_font_color)

main = Browser.element
  { init = init
  , update = update
  , view = view
  , subscriptions = subscriptions
  }

-- MODEL
type alias Model
  = { username : String
    , password : String
    , repeated_password : String
    , first_name : String
    , last_name : String
    , email : String
    , accepted : Bool
    , message : String
    , enable_regstration : Bool
    , registration_view : Bool
    , width : Int
    , height : Int
    }

init : (Bool, Int, Int) -> (Model, Cmd Msg)
init (register, width, height)
        = ({ username = ""
          , password = ""
          , repeated_password = ""
          , first_name = ""
          , last_name = ""
          , email = ""
          , accepted = False
          , message = ""
          , enable_regstration = register
          , width = width
          , height = height
          , registration_view = False
          }, Cmd.none)

-- UPDATE
type Field
  = Username
  | Password
  | Repeated_Password
  | First_Name
  | Last_Name
  | Email

type Msg
  = Change Field String
  | ToggleCheckbox Bool
  | Submit { register : Bool }
  | Recv_Response Bool (Result Http.Error ())
  | Resize Int Int
  | Enable_Register

update : Msg -> Model -> (Model, Cmd Msg)
update msg model
  = case msg of
      Change field new ->
        (case field of
          Username -> { model | username = new }
          Password -> { model | password = new }
          Repeated_Password -> { model | repeated_password = new }
          First_Name -> { model | first_name = new }
          Last_Name -> { model | last_name = new }
          Email -> { model | email = new }
        , Cmd.none)
      ToggleCheckbox value
        -> ({ model | accepted = value } , Cmd.none)
      Submit { register } ->
        let path = if register
                    then ["register"]
                    else ["login"]
            query = if register
                      then [ Builder.string "password" model.password
                           , Builder.string "username" model.username
                           , Builder.string "first_name" model.first_name
                           , Builder.string "last_name" model.last_name
                           , Builder.string "email" model.email
                           , Builder.string "accept" <| if model.accepted then "1" else "0"
                           ]

                      else [Builder.string "password" model.password, Builder.string "username" model.username]
        in if not register || ((model.repeated_password == model.password) && model.accepted)
            then (model, Http.post
                          { url = Builder.absolute path query
                          , body = Http.emptyBody
                          , expect = Http.expectWhatever (Recv_Response register)
                          })
            else ({model | message = "Passwords do not match or terms not accepted?"}, Cmd.none)

      Recv_Response register result ->
        case result of
          Err _ -> ({ model | message = "Invalid password / username? Some data not provided?" }, Cmd.none)
          Ok _ -> (model, redirect ())
      Resize width height -> ({ model | width = width, height = height }, Cmd.none)
      Enable_Register -> ({ model | registration_view = True }, Cmd.none)

port redirect : () -> Cmd msg

-- SUBSCRIPTIONS
subscriptions model = Sub.batch [Browser.Events.onResize Resize]

-- VIEW
view : Model -> Html Msg
view model =
  Element.layout []
  <| if model.registration_view && model.enable_regstration
      then view_registration model
      else Element.column []
        [ view_login model
        , if model.enable_regstration
            then Colors.button []
              { onPress = Just Enable_Register
              , label = text "Register"
              }
            else Element.none
        ]

view_login : Model -> Element Msg
view_login model
 = Element.column [width <| fillPortion 1, height Element.shrink, Element.paddingXY 50 0]
      [ el [Font.size 24] <| text "Login"
      , text model.message
      , Input.username [Input.focusedOnLoad
                       , onEnter <| Submit { register = False }
                       ]
          { onChange = Change Username
          , text = model.username
          , placeholder = Just << Input.placeholder [] <| text "Username"
          , label = Input.labelAbove [] <| text "Username"
          }
      , Input.currentPassword [onEnter <| Submit { register = False }]
          { onChange = Change Password
          , text = model.password
          , placeholder = Just << Input.placeholder [] <| text "Password"
          , label = Input.labelAbove [] <| text "Password"
          , show = False
          }
      , Input.button
          [ Background.color button_color
          , Border.color button_color
          , Border.width 10
          , Font.color button_font_color
          , centerX
          ]
          { onPress = Just <| Submit { register = False }
          , label = text "Login"
          }
      ]

view_registration : Model -> Element Msg
view_registration model
  = Element.column [width <| fillPortion 2, height Element.shrink]
    [ el [Font.size 24, Font.center] <| text "Register"
    , Input.username []
        { onChange = Change Username
        , text = model.username
        , placeholder = Just << Input.placeholder [] <| text "Username"
        , label = Input.labelAbove [] <| text "Username"
        }
    , Input.newPassword []
        { onChange = Change Password
        , text = model.password
        , placeholder = Just << Input.placeholder [] <| text "Password"
        , label = Input.labelAbove [] <| text "Password"
        , show = False
        }
    , Input.newPassword []
        { onChange = Change Repeated_Password
        , text = model.repeated_password
        , placeholder = Just << Input.placeholder [] <| text "Repeated Password"
        , label = Input.labelAbove [] <| text "Repeated Password"
        , show = False
        }
    , Input.text []
        { onChange = Change First_Name
        , text = model.first_name
        , placeholder = Just << Input.placeholder [] <| text "First Name"
        , label = Input.labelAbove [] <| text "(legal) First Name"
        }
    , Input.text []
        { onChange = Change Last_Name
        , text = model.last_name
        , placeholder = Just << Input.placeholder [] <| text "Last Name"
        , label = Input.labelAbove [] <| paragraph [] [text "(legal) Last Name" ]
        }
    , Input.email [onEnter <| Submit { register = True}]
        { onChange = Change Email
        , text = model.email
        , placeholder = Just << Input.placeholder [] << text <|
            if model.first_name /= "" && model.last_name /= ""
              then model.first_name ++ "." ++ model.last_name ++ "@example.org"
              else "personal@example.org"
        , label = Input.labelAbove [] <| text "Email"
        }
    , Input.checkbox []
        { onChange = ToggleCheckbox
        , icon = Input.defaultCheckbox
        , checked = model.accepted
        , label = Input.labelRight [] <| paragraph []
                    [Element.newTabLink []
                        { url = "/agreements/Registration"
                        , label = text "I accept Terms of Registration"
                        }
                    ]
        }
    , Input.button
        [ Background.color button_color
        , Border.color button_color
        , Border.width 10
        , Font.color button_font_color
        , centerX
        ]
        { onPress = Just <| Submit { register = True }
        , label = text "Register"
        }
    ]

-- EVENT HANDLERS
onEnter : msg -> Element.Attribute msg
onEnter msg =
  Element.htmlAttribute
    (Html.Events.on "keyup"
      (Decode.field "key" Decode.string
        |> Decode.andThen
          (\key ->
              if key == "Enter"
                  then Decode.succeed msg
                  else Decode.fail "Not the enter key"
          )
      )
    )
