module DemNet.User exposing ( User
                            , Predictions
                            , decoder
                            , init
                            , fetch
                            , support
                            , neutral
                            , oppose
                            , is_supporting
                            , is_opposing
                            , upload_bio
                            )
import Json.Decode as D
import Json.Encode as E
import Http
import Url.Builder as Builder

-- MODEL
type alias User =
  { username : String
  , authentic : Bool
  , accuracy : Float
  , bio : String
  , predictions_on : Maybe Predictions
  , predictions : Maybe Predictions
  }

type Predictions = Predictions { for : List User, against : List User }

-- JSON
decoder : D.Decoder User
decoder =
  D.map6 User
    (D.field "username" D.string)
    (D.field "authentic" D.bool)
    (D.field "accuracy" D.float)
    (D.field "bio" D.string)
    (D.nullable <| predictions_decoder "supporters" "opponents")
    (D.nullable <| predictions_decoder "supporting" "opposing")

predictions_decoder : String -> String -> D.Decoder Predictions
predictions_decoder for_field against_field
  = D.map2 (\f a -> Predictions { for = Maybe.withDefault [] f, against = Maybe.withDefault [] a })
      (D.field for_field << D.nullable <| D.list <| D.lazy (\_ -> decoder))
      (D.field against_field << D.nullable << D.list <| D.lazy (\_ -> decoder))


-- INIT
init : String -> User
init username
  = { username = username
    , authentic = False
    , accuracy = 0.0
    , bio = ""
    , predictions_on = Nothing
    , predictions = Nothing
    }

-- HTTP
fetch : Bool -> String -> (Result Http.Error User -> msg) -> Cmd msg
fetch predictions_on username msg
  = let query = if predictions_on
                  then [Builder.string "predictions_on" "1"]
                  else [Builder.string "predictions" "1"]
        query_ = query ++ [Builder.string "recalc" "1"]
    in  Http.get { url = Builder.absolute ["api", "v0", "user", username] query_
                 , expect = Http.expectJson msg decoder
                 }

upload_bio : User -> (Result Http.Error () -> msg) -> Cmd msg
upload_bio user msg
  = Http.request
      { method = "PUT"
      , headers = []
      , url = Builder.absolute ["api", "v0", "user", user.username] [Builder.string "bio" user.bio]
      , body = Http.emptyBody
      , expect = Http.expectWhatever msg
      , timeout = Nothing
      , tracker = Nothing
      }

-- VERIFICATION ACTIONS
type Action
  = Support
  | Neutral
  | Oppose

act : Action -> User -> User -> (Result Http.Error User -> msg) -> Cmd msg
act action user self msg
  = let parameter = E.encode 0 (E.list (\u -> E.string u.username) [user])
        action_str = case action of
                  Support -> "support"
                  Neutral -> "neutral"
                  Oppose -> "oppose"
        query = [Builder.string action_str parameter]
    in Http.request
        { method = "PUT"
        , headers = []
        , url = Builder.absolute ["api", "v0", "user", self.username] query
        , body = Http.emptyBody
        , expect = Http.expectJson msg decoder
        , timeout = Nothing
        , tracker = Nothing
        }

support : User -> User -> (Result Http.Error User -> msg) -> Cmd msg
support = act Support

neutral : User -> User -> (Result Http.Error User -> msg) -> Cmd msg
neutral = act Neutral

oppose : User -> User -> (Result Http.Error User -> msg) -> Cmd msg
oppose = act Oppose

-- PREDICATES
is_supporting : User -> User -> Bool
is_supporting user self
 = case self.predictions of
    Just (Predictions { for }) -> List.member user.username <| List.map (\u -> u.username) for
    Nothing -> False

is_opposing : User -> User -> Bool
is_opposing user self
  = case self.predictions of
      Just (Predictions { against }) -> List.member user.username <| List.map (\u -> u.username) against
      Nothing -> False
