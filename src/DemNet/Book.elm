module DemNet.Book exposing ( Book(..)
                            , empty
                            , decoder
                            , encode
                            , get_rules
                            , accept
                            , view
                            )
{-| Module for working with the Rules API.
# Definition
@docs Book
@docs empty

# JSON
@docs decoder
@docs encode

# HTTP
@docs get_rules

# View
@docs view
-}
import Http
import Json.Decode as D
import Json.Encode as Encode
import Parser exposing (Parser)
import Element exposing (Element)
import Element.Font as Font
import DemNet.Date as Date exposing (Date)

{-| Synonymous with a rule.
-}
type Book
  = Book
      { name : String
      , text : String
      , since_in_force : Date
      , accepted : Maybe Bool
      , view_text : Bool
      }

{-| Empty Book
-}
empty : Book
empty = Book { name = ""
             , text = ""
             , since_in_force = Date.null
             , accepted = Nothing
             , view_text = False
             }

{-| A Book:
```json
{ "name" : <NAME>
, "text" : <TEXT>
, "since" : <Date of enactment>
, "accepted" : <Has the authenticated user accepted this book?>
}
```
-}
decoder : D.Decoder Book
decoder =
  D.map4 (\n t s a -> Book { name = n
                           , text = t
                           , since_in_force = Maybe.withDefault Date.null (Result.withDefault (Just Date.null) <| Parser.run Date.parser s)
                           , accepted = a
                           , view_text = a == Nothing || a == Just False
                           })
    (D.field "name" D.string)
    (D.field "text" D.string)
    (D.field "since" D.string)
    (D.field "accepted" <| D.nullable D.bool)

{-| Encode a Book. Inverse of the decoder.
-}
encode : Book -> Encode.Value
encode (Book book)
  = Encode.object
      [ ("name", Encode.string book.name)
      , ("text", Encode.string book.text)
      ]
{-| Get the rules.
-}
get_rules : (Result Http.Error (List Book) -> msg) -> Cmd msg
get_rules msg
  = Http.get
        { url = "/api/v0/rules"
        , expect = Http.expectJson msg (D.list decoder)
        }

{-| Accept a book as the authenticated user.
-}
accept : (Book -> Result Http.Error Int -> msg) -> Book -> Cmd msg
accept msg (Book book)
  = Http.post
      { url = "/api/v0/rules/" ++ book.name
      , body = Http.emptyBody
      , expect = Http.expectJson (msg <| Book book) D.int
      }

{-| View the Book
-}
view : Book -> Element msg
view (Book book)
  = Element.column
      [ Element.centerX
      , Element.width Element.fill
      ]
      [ Element.el [Font.size 24] <| Element.text book.name
      , Element.paragraph [] [Element.text book.text ]
      ]
