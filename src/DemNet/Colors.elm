module DemNet.Colors exposing (..)
import Element exposing (Element)
import Element.Input as Input
import Element.Font as Font
import Element.Border as Border
import Element.Background as  Background

button_color = Element.rgb255 86 90 96
button_font_color = Element.rgb255 255 255 255

button : List (Element.Attribute msg) -> { onPress : Maybe msg, label : Element msg } -> Element msg
button attrs { label, onPress}
  = Input.button
      ([ Font.color button_font_color
       , Border.width 5
       , Background.color button_color
       ] ++ attrs)
      { onPress = onPress
      , label = label
      }

details_color = Element.rgb255 199 204 197
details_attrs =  [ Border.width 5
                 , Border.color <| details_color
                 , Background.color <| details_color
                 ]
details_open : List (Element.Attribute msg) -> { onPress : Maybe msg, label : Element msg } -> Element msg
details_open attrs { label, onPress }
  = Input.button
      (details_attrs ++ attrs)
      { onPress = onPress
      , label = label
      }
