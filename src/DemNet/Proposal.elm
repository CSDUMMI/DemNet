module DemNet.Proposal exposing (Proposal, empty, propose, add)
{-| Module for representing and working with the Proposals.
# Definitions
@docs Proposal
@docs empty

# Manipulation
@docs add

# HTTP
@docs propose
@docs get_proposals
-}
import DemNet.Book as Book exposing (Book(..))

import Dict exposing (Dict)
import Url
import Url.Builder as Builder

import Json.Encode as Encode
import Json.Decode as D

import Http
import Element
import Element.Input as Input

{-| A Proposal may have two parts:
A change to the Rule Books. (textProposals)
or a change to the code. (codeProposal)
Of the later there may only ever be one
(Generated using git diff for git apply),
while of the other there maybe as many
as there is space to store them.

A proposal of course also has a description
and a title for the users to inform them
about the proposal.
-}
type alias Proposal
  = { textProposals : Dict String Book
    , codeProposal : String
    , description : String
    , title : String
    }

{-| Predicate checking if a proposal is empty,
wherein only text and code Proposals are considered.
Only if this predicate is False can a proposal
be proposed.
-}
empty : Proposal -> Bool
empty proposal
  = proposal.textProposals == Dict.empty && proposal.codeProposal == ""

{-| Add a book to the textProposals
-}
add : Book -> Proposal -> Proposal
add (Book book) proposal
  = { proposal | textProposals = Dict.insert book.name (Book book) proposal.textProposals }

{-| Encode a proposal for proposing.
-}
encode : Proposal -> Encode.Value
encode proposal
  = Encode.object
      [("title", Encode.string proposal.title)
      ,("description", Encode.string proposal.description)
      ,("code_proposal", Encode.string proposal.codeProposal)
      ,("book_proposals", Encode.list Book.encode <| Dict.values proposal.textProposals)
      ]

{-| Propose a Proposal to the server.
There can be three kinds of errors here:
1. An HTTP error
2. An error on the server side.
In which case there is an error code returned.
For example, the server will return an error code, if
there is not currently a proposal phase, the server will say so.
3. An error on the client side.
In which case the proposal is not valid, maybe empty.
-}
propose : (Result Http.Error Int -> msg) -> Proposal -> Cmd msg
propose msg proposal
  = let book_proposals = Dict.values proposal.textProposals
                          |> Encode.list Book.encode
                          |> Encode.encode 0
        body = encode proposal
    in Http.post { url = Builder.absolute ["api","v0","vote"] []
                 , body = Http.jsonBody body
                 , expect = Http.expectJson msg D.int
                 }
