module DemNet.File exposing (File, decoder, upload, delete, get)
import Json.Decode as D
import File as F
import Url.Builder as Builder
import Http

type alias File =
  { cid : String
  , url : String
  , filename : Maybe String
  }

-- JSON
decoder : D.Decoder File
decoder =
  D.map3 File
    (D.field "cid" D.string)
    (D.field "url" D.string)
    (D.field "name" <| D.nullable D.string)

-- HTTP
url = "/api/v0/files"

upload : (Result Http.Error File -> msg) -> F.File -> Cmd msg
upload msg file
  = Http.post
     { url = url
     , body = Http.fileBody file
     , expect = Http.expectJson msg decoder
     }

delete : (File -> Result Http.Error Int -> msg) -> File -> Cmd msg
delete msg file
  = Http.request
      { method = "DELETE"
      , headers = []
      , url = Builder.absolute ["api", "v0", "files"] [Builder.string "cid" file.cid]
      , body = Http.emptyBody
      , expect = Http.expectJson (msg file) D.int
      , timeout = Nothing
      , tracker = Nothing
      }

get : (Result Http.Error (List File) -> msg) -> Cmd msg
get msg =
  Http.get
    { url = url
    , expect = Http.expectJson msg (D.list decoder)
    }
