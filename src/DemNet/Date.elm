module DemNet.Date exposing (Date, parser, encode, null, nice_encode)
{-| This Module implements a simple date type
for the purposes of DemNet.

# Definition
@docs Date

@docs null

# From and to String
@docs parser
@docs validDate
@docs encode

-}
import Parser exposing (Parser, (|.), (|=), succeed, symbol, int)

{-| A date consisting of a year, a month and a day.
-}
type alias Date
  = { year : Int
    , month : Int
    , day : Int
    }

{-| Not actually null,
just the least possible value.
-}
null : Date
null = { year = 2020, month = 10, day = 30 }

{-|
-}
parser : Parser (Maybe Date)
parser =
  succeed (\year month day -> validDate year month day)
    |= int
    |. symbol "-"
    |= int
    |. symbol "-"
    |= int

validDate : Int -> Int -> Int -> Maybe Date
validDate year month day
  = if month > 0 && month <= 12 && day > 0 && day <= 31
      then Just (Date year month day)
      else Nothing

encode : Date -> String
encode date = String.fromInt date.year ++ "-" ++ String.fromInt date.month ++ "-" ++ String.fromInt date.day

nice_encode : Date -> String
nice_encode date
  = String.fromInt date.day ++ "." ++ String.fromInt date.month ++ "." ++ String.fromInt date.year
