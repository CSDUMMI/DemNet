module DemNet.Errors exposing ( Errors
                              , empty
                              , get
                              , decoder
                              , toText
                              , fromText
                              )
{-| Handeling error codes
from the DemNet API v0.

# Definition
@docs Errors
@docs get

# JSON
@docs decoder

# Utils
@docs toText
@docs fromText
-}
import Http
import Url.Builder as Builder
import Json.Decode as D
import Dict exposing (Dict)

type alias Errors = Dict String Int

empty : Errors
empty = Dict.empty

{-| Get the errors from the API Endpoint.
-}
get : (Result Http.Error Errors -> msg) -> Cmd msg
get msg
  = Http.get
      { url = Builder.absolute ["api", "v0", "errors"] []
      , expect = Http.expectJson msg decoder
      }

{-| Decoding a Dictionary of error text to int.
-}
decoder : D.Decoder Errors
decoder = D.dict D.int

{-| reverse a key-value mapping
to value-key.
-}
reverse : Errors -> Dict Int String
reverse k
  = let keys = Dict.keys k
        values = Dict.values k
    in Dict.fromList <| List.map2 Tuple.pair values keys

{-| Get the text based on the code.
-}
toText : Int -> Errors -> Maybe String
toText code errors
  = Dict.get code <| reverse errors

{-| Get the code from the text
-}
fromText : String -> Errors -> Maybe Int
fromText text errors = Dict.get text errors
