#!/usr/bin/env bash

if [[ -e $GIT_REPOSITORY ]]; then
  rm -rf $GIT_REPOSITORY
fi

git clone -b master --single-branch https://oauth2:$GIT_ACCESS_TOKEN@$DEMNET_LOCATION_WITHOUT_PROTOCOL $GIT_REPOSITORY
